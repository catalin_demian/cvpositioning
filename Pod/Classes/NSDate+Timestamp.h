//
//  NSDate+Timestamp.h
//  Convoyz
//
//  Created by Andrei Dumitru on 7/1/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Timestamp)

- (NSTimeInterval)timestamp;

@end
