//
//  Session+CoreData.h
//  Convoyz
//
//  Created by Andrei Dumitru on 5/11/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPSession.h"

@interface CVPSession (CoreData)

+ (void)setupCoreDataStack;
+ (void)clearDatabase;

+ (NSURL *)storeURL;

+ (void)saveDataInBackground:(id (^)(NSManagedObjectContext *localContext))block completionBlock:(void (^)(id result, NSError *error))completionBlock;

@end
