//
//  Session.m
//  Convoyz
//
//  Created by Alexandru Tudose on 16.02.2015.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPSession.h"
#import "CVPConstants.h"
#import <CoreLocation/CoreLocation.h>

@implementation CVPSession

+ (BOOL)showAlerts {
    NSNumber *showAlerts = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.showAlerts];
    if (!showAlerts) {
        showAlerts = @(NO);
        [self setShowAlerts:showAlerts.boolValue];
    }
    return showAlerts.boolValue;
}

+ (NSDate *)applicationKillDate {
    NSDate *date = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.applicationKillDate];
    return date;
}

+ (NSNumber *)activityType {
    NSNumber *type = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.activityType];
    if (!type) {
        type = @(CLActivityTypeAutomotiveNavigation);
        [self setActivityType:type];
    }
    return type;
}

+ (NSNumber *)updateDistance {
    NSNumber *distance = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.updateDistance];
    if (!distance) {
        distance = @(CVPDefaultUpdateDistance);
        [self setUpdateDistance:distance];
    }
    return distance;
}

+ (BOOL)significantChanges {
    NSNumber *significantChanges = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.significantChanges];
    if (!significantChanges) {
        significantChanges = @(NO);
        [self setSignificantChanges:significantChanges.boolValue];
    }
    return significantChanges.boolValue;
}

+ (BOOL)geofenceEnabled {
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.geofenceEnabled];
}

+ (BOOL)visitsEnabled {
    NSNumber *visits = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.visitsEnabled];
    if (!visits) {
        visits = @(YES);
        [self setSignificantChanges:visits.boolValue];
    }
    return visits.boolValue;
}

+ (NSNumber *)geofenceRadius {
    NSNumber *distance = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.geofenceRadius];
    if (!distance) {
        distance = @(CVPDefaultGeofenceRadius);
        [self setGeofenceRadius:distance];
    }
    return distance;
}

+ (BOOL)deferEnabled {
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.deferEnabled];
}

+ (BOOL)autoPauseLocationByiOSEnabled {
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.autoPauseLocationByiOS];
}

+ (BOOL)continousMonitoringEnabled {
    if (! [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.continousMonitoring]) {
        [self setContinousMonitoringEnabled:YES];
    }
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.continousMonitoring];
}

+ (NSNumber *)deferDistance {
    NSNumber *distance = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.deferDistance];
    if (!distance) {
        distance = @(CVPDefaultDeferDistancte);
        [self setDeferDistance:distance];
    }
    return distance;
}

+ (NSNumber *)deferTimeInterval {
    NSNumber *interval = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.deferTimeInterval];
    if (!interval) {
        interval = @(CVPDefaultDeferTimeInterval);
        [self setDeferTimeInterval:interval];
    }
    return interval;
}

+ (NSNumber *)restartMonitoringTimeInterval {
    NSNumber *interval = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.restartMonitoringTimeInterval];
    if (!interval) {
        interval = @(CVPRestartMonitoringAfterTimeInterval);
        [self setRestartMonitoringTimeInterval:interval];
    }
    return interval;
}

+ (BOOL)decreaseAccuracyEnabled {
    if (! [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.decreaseAccuracyEnabled]) {
        [self setDecreaseAccuracyEnabled:YES];
    }
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.decreaseAccuracyEnabled];
}

+ (NSNumber *)checkAccuracyTimeInterval {
    NSNumber *interval = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.checkAccuracyTimeInterval];
    if (!interval) {
        interval = @(CVPCheckAccuracyTimeInterval);
    }
    return interval;
}

+ (NSNumber *)monitoringBufferInterval {
    NSNumber *interval = [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.monitoringBufferTimeInterval];
    if (!interval) {
        interval = @(CVPMonitoringBufferTimeInterval);
    }
    return interval;
}

+ (BOOL)isDelayedBackgroundSync {
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.isDelayedBackgroundSync];
}

+ (NSNumber *)locationAccuracy {
    NSNumber *accuracy = [[NSUserDefaults standardUserDefaults] objectForKey:CVPSessionSpace.accuracy];
    if (! accuracy) {
        accuracy = @(kCLLocationAccuracyNearestTenMeters);
    }
    
    return accuracy;
}

+ (BOOL)motionProcessorEnabled {
    if (! [[NSUserDefaults standardUserDefaults] valueForKey:CVPSessionSpace.motionProcessorEnabled]) {
        [self setMotionProcessorEnabled:YES];
    }
    return [[NSUserDefaults standardUserDefaults] boolForKey:CVPSessionSpace.motionProcessorEnabled];
}

+ (void)setShowAlerts:(BOOL)showAlerts {
    [[NSUserDefaults standardUserDefaults] setBool:showAlerts forKey:CVPSessionSpace.showAlerts];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setApplicationKillDate:(NSDate *)date {
    [[NSUserDefaults standardUserDefaults] setValue:date forKey:CVPSessionSpace.applicationKillDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setActivityType:(NSNumber *)activityType {
    [[NSUserDefaults standardUserDefaults] setValue:activityType forKey:CVPSessionSpace.activityType];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUpdateDistance:(NSNumber *)updateDistance {
    [[NSUserDefaults standardUserDefaults] setValue:updateDistance forKey:CVPSessionSpace.updateDistance];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setSignificantChanges:(BOOL)significantChanges {
    [[NSUserDefaults standardUserDefaults] setBool:significantChanges forKey:CVPSessionSpace.significantChanges];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setGeofenceEnabled:(BOOL)geofencesEnabled {
    [[NSUserDefaults standardUserDefaults] setBool:geofencesEnabled forKey:CVPSessionSpace.geofenceEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setVisitsEnabled:(BOOL)visitsEnabled {
    [[NSUserDefaults standardUserDefaults] setBool:visitsEnabled forKey:CVPSessionSpace.visitsEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setGeofenceRadius:(NSNumber *)geofenceRadius {
    [[NSUserDefaults standardUserDefaults] setValue:geofenceRadius forKey:CVPSessionSpace.geofenceRadius];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setDeferEnabled:(BOOL)deferEnabled {
    [[NSUserDefaults standardUserDefaults] setBool:deferEnabled forKey:CVPSessionSpace.deferEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setDeferDistance:(NSNumber *)deferDistance {
    [[NSUserDefaults standardUserDefaults] setValue:deferDistance forKey:CVPSessionSpace.deferDistance];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setDeferTimeInterval:(NSNumber *)deferTimeInterval {
    [[NSUserDefaults standardUserDefaults] setValue:deferTimeInterval forKey:CVPSessionSpace.deferTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setRestartMonitoringTimeInterval:(NSNumber *)restartMonitoringTimeInterval {
    [[NSUserDefaults standardUserDefaults] setValue:restartMonitoringTimeInterval forKey:CVPSessionSpace.restartMonitoringTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setAutoPauseLocationByiOSEnabled:(BOOL)autoPause {
    [[NSUserDefaults standardUserDefaults] setValue:@(autoPause) forKey:CVPSessionSpace.autoPauseLocationByiOS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setContinousMonitoringEnabled:(BOOL)contiousEnabled {
    [[NSUserDefaults standardUserDefaults] setValue:@(contiousEnabled) forKey:CVPSessionSpace.continousMonitoring];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setDecreaseAccuracyEnabled:(BOOL)decreaseAccuracyEnabled {
    [[NSUserDefaults standardUserDefaults] setValue:@(decreaseAccuracyEnabled) forKey:CVPSessionSpace.decreaseAccuracyEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setCheckAccuracyTimeInterval:(NSNumber *)checkAccuracyTimeInterval {
    [[NSUserDefaults standardUserDefaults] setValue:checkAccuracyTimeInterval forKey:CVPSessionSpace.checkAccuracyTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setMonitoringBufferTimeInterval:(NSNumber *)monitoringBufferTimeInterval {
    [[NSUserDefaults standardUserDefaults] setValue:monitoringBufferTimeInterval forKey:CVPSessionSpace.monitoringBufferTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setIsDelayedBackgroundSync:(BOOL)isDelayed {
    [[NSUserDefaults standardUserDefaults] setBool:isDelayed forKey:CVPSessionSpace.isDelayedBackgroundSync];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setLocationAccuracy:(NSNumber *)accuracy {
    [[NSUserDefaults standardUserDefaults] setObject:accuracy forKey:CVPSessionSpace.accuracy];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setMotionProcessorEnabled:(BOOL)enabled {
    [[NSUserDefaults standardUserDefaults] setBool:enabled forKey:CVPSessionSpace.motionProcessorEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)removeUserDetails {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.showAlerts];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.updateDistance];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.significantChanges];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.geofenceEnabled];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.geofenceRadius];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.deferEnabled];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.deferDistance];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CVPSessionSpace.deferTimeInterval];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

