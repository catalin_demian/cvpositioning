//
//  NSManagedObjectContext+Save.m
//  Convoyz
//
//  Created by Andrei Dumitru on 9/22/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "NSManagedObjectContext+Save.h"

@implementation NSManagedObjectContext (Save)

- (nonnull NSArray *)moveBackgroundObjects:(nonnull NSArray *)managedObjects {
    NSMutableArray *newManagedObjects = [NSMutableArray arrayWithCapacity:managedObjects.count];
    for (NSManagedObject *backgroundObject in managedObjects) {
        [newManagedObjects addObject:[self objectWithID:[backgroundObject objectID]]];
    }
    return newManagedObjects;
}

@end
