//
//  NSManagedObjectContext+Save.h
//  Convoyz
//
//  Created by Andrei Dumitru on 9/22/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Save)

- (nonnull NSArray *)moveBackgroundObjects:(nonnull NSArray *)managedObjects;

@end
