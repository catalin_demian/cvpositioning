//
//  System+Network.h
//  Convoyz
//
//  Created by Andrei Dumitru on 5/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

typedef NS_ENUM(NSUInteger, NetworkStatus) {
    NotReachable = 0,
    ReachableViaWiFi,
    ReachableViaWWAN
};

@interface SystemManager (Network)

@property (readonly, nonatomic) NetworkStatus networkStatus;
@property (readonly, nonatomic) NSString *networkStatusString;

@property (readonly, nonatomic) NSDictionary *BSSIDDictionary;
@property (readonly, nonatomic) NSString *BSSIDString;

@end
