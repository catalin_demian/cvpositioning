//
//  System.m
//  Convoyz
//
//  Created by Andrei Dumitru on 5/20/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

@interface SystemManager ()

@property (assign, nonatomic) BOOL proximityState;

@end

@implementation SystemManager

+ (instancetype)instance {
    static SystemManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        //TODO: Check the proximity use case.
        [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceProximityStateDidChange:) name:UIDeviceProximityStateDidChangeNotification object:nil];
    }
    return self;
}

#pragma mark - Hardware

- (float)batteryLevel {
    //Enable battery monitoring only when you need it.
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    float bateryLevel = [UIDevice currentDevice].batteryLevel;
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:NO];
    return bateryLevel;
}

- (CGFloat)screenBrightness {
    return [UIScreen mainScreen].brightness;
}

- (UIApplicationState)applicationState {
    return [[UIApplication sharedApplication] applicationState];
}

- (NSString *)applicationStateString {
    NSString *stateString = nil;
    switch (self.applicationState) {
        case UIApplicationStateActive:
            stateString = @"ApplicationStateActive";
            break;
        case UIApplicationStateInactive:
            stateString = @"ApplicationStateInactive";
            break;
        case UIApplicationStateBackground:
            stateString = @"ApplicationStateBackground";
            break;
        default:
            break;
    }
    return stateString;
}

#pragma mark - Notifications

- (void)deviceProximityStateDidChange:(NSNotification *)notification {
    self.proximityState = [UIDevice currentDevice].proximityState;
}

@end
