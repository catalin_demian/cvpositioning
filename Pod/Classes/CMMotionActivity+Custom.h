//
//  CMMotionActivity+Custom.h
//  Convoyz
//
//  Created by Andrei Dumitru on 7/27/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface CMMotionActivity (Custom)

@property (readonly, nonatomic) NSString *confidenceString;

@end
