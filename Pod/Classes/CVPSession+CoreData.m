//
//  Session+CoreData.m
//  Convoyz
//
//  Created by Andrei Dumitru on 5/11/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPSession+CoreData.h"
#import "NSManagedObjectContext+Save.h"

#define TTParseErrorCode 1

@implementation CVPSession (CoreData)

static NSString * const TTStoreName = @"TTStore.sqlite";

+ (void)setupCoreDataStack {
    [MagicalRecord setupCoreDataStackWithStoreNamed:TTStoreName];
    
    BOOL failedToSetupCoreData = [[[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] persistentStores] count] < 1;
    if (failedToSetupCoreData) {
        [MagicalRecord cleanUp];
        
        // remove sqlite file and try again.
        NSError *deleteError = nil;
        [[NSFileManager defaultManager] removeItemAtURL:[NSPersistentStore MR_urlForStoreName:TTStoreName] error:&deleteError];
        if (deleteError) {
            TTLog(@"%@", deleteError);
        }
        
        [MagicalRecord setupCoreDataStackWithStoreNamed:TTStoreName];
    }
    
    TTDebugOnly(^{
        for (NSEntityDescription *entity in [[[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] managedObjectModel] entities]) {
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[entity name]];
            NSLog(@"%@ - objects count - %lu",  [entity name], (unsigned long)[[NSManagedObjectContext MR_defaultContext] countForFetchRequest:fetchRequest error:nil]);
        }
    });
}

+ (void)clearDatabase {
    TTLog(@"clearDatabase");
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSEntityDescription *entity in [[[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] managedObjectModel] entities]) {
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[entity name]];
            fetchRequest.includesPropertyValues = NO;
            fetchRequest.includesSubentities = NO;
            
            NSError *error = nil;
            NSArray *items = [localContext executeFetchRequest:fetchRequest error:&error];
            if (error) {
                TTLog(@"Error requesting items from Core Data: %@", [error localizedDescription]);
            }
            TTLog(@"%@ - objects count - %ld",  [entity name], (unsigned long)items.count);
            
            for (NSManagedObject *managedObject in items) {
                [localContext deleteObject:managedObject];
            }
        }
    }];
}

+ (NSURL *)storeURL {
    return [NSPersistentStore MR_urlForStoreName:TTStoreName];
}

+ (void)saveDataInBackground:(id (^)(NSManagedObjectContext *localContext))block completionBlock:(void (^)(id result, NSError *error))completionBlock {
    __block NSError *coreDataError = nil;
    __block id parsedResponse = nil;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        @try {
            parsedResponse = block(localContext);
        }
        @catch (NSException *exception) {
            [localContext reset];
            coreDataError = [NSError errorWithDomain:exception.name code:TTParseErrorCode userInfo:exception.userInfo];
            TTLog(@"Core Data exception: %@", exception);
        }
    } completion:^(BOOL success, NSError *error) {
        NSArray *response = nil;
        if (parsedResponse) {
            BOOL singleObject = ! [parsedResponse isKindOfClass:[NSArray class]];
            if (singleObject) {
                parsedResponse = @[parsedResponse];
            }
            
            response = [[NSManagedObjectContext MR_defaultContext] moveBackgroundObjects:parsedResponse];
            
            if (singleObject) {
                response = [response lastObject];
            }
        }
        if (completionBlock) {
            completionBlock(response, error ? error : coreDataError);
        }
    }];
}

@end
