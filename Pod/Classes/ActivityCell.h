//
//  ActivityCell.h
//  Positioning
//
//  Created by Alexandru Tudose on 16/10/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@end
