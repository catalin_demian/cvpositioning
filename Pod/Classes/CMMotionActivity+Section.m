//
//  CMMotionActivity+Section.m
//  Positioning
//
//  Created by Alexandru Tudose on 19/01/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CMMotionActivity+Section.h"

@implementation CMMotionActivity (Section)

- (NSString *)sectionTitle {
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = [NSString stringWithFormat:@"dd MMMM"];
    });
    
    return [dateFormatter stringFromDate:self.startDate];
}

@end
