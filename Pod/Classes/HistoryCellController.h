//
//  HistoryCellController.h
//  Positioning
//
//  Created by Andrei Dumitru on 10/7/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Tapptitude/TTBlockCollectionCellController.h>

@interface HistoryCellController : TTBlockCollectionCellController

@end
