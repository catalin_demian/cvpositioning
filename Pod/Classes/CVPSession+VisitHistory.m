//
//  CVPSession+VisitHistory.m
//  Pods
//
//  Created by Catalin Demian on 2/17/16.
//
//

#import "CVPSession+VisitHistory.h"
#import "CVPSession+CoreData.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation CVPSession (VisitHistory)

+ (void)saveVisit:(CLVisit *)visit withCallback:(void(^)(CVPVisit *visit, NSError *error))callback {
    [self saveDataInBackground:^id(NSManagedObjectContext *localContext) {
        CVPVisit *savedVisit = [CVPVisit insertInManagedObjectContext:localContext];
        savedVisit.dateTime = [NSDate date];
        savedVisit.departureDate = visit.departureDate;
        savedVisit.arrivalDate = visit.arrivalDate;
        savedVisit.latitudeValue = visit.coordinate.latitude;
        savedVisit.longitudeValue = visit.coordinate.longitude;
        savedVisit.accuracyValue = visit.horizontalAccuracy;
        return savedVisit;
    } completionBlock:^(id result, NSError *error) {
        if (callback) {
            callback(result, error);
        }
    }];
}

+ (void)deleteOldVisitsWithCallback:(void(^)(NSError *error))callback {
    [self saveDataInBackground:^id(NSManagedObjectContext *localContext) {
        NSDate *date = [NSDate dateWithTimeInterval:-CVPDayTimeInterval*3 sinceDate:[NSDate date]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateTime < %@", date];
        [CVPVisit MR_deleteAllMatchingPredicate:predicate inContext:localContext];
        return nil;
    } completionBlock:^(id result, NSError *error) {
        if (callback) {
            callback(error);
        }
    }];
}

+ (NSArray *)fetchRecentVisits {
    return [CVPVisit MR_findAllSortedBy:@"dateTime" ascending:YES];
}

@end
