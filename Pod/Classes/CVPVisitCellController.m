//
//  CVPVisitCellController.m
//  Pods
//
//  Created by Catalin Demian on 2/19/16.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "CVPVisitCellController.h"
#import "CVPVisitCell.h"
#import "CVPVisit.h"

@implementation CVPVisitCellController

- (id)init {
    if ( ( self = [super initWithCellNibName:@"CVPVisitCell"
                                  contentSize:CGSizeMake(-1, 50)]) ) {
        self.minimumInteritemSpacing = 0.0;
        self.minimumLineSpacing = 0.0;
        self.sectionInset = UIEdgeInsetsZero;
    }
    return self;
}

- (BOOL)acceptsContent:(id)content {
    return [content isKindOfClass:[CVPVisit class]];
}

@end
