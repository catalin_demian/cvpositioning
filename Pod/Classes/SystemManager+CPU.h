//
//  System+CPU.h
//  Convoyz
//
//  Created by Andrei Dumitru on 5/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

@interface SystemManager (CPU)

//CPU usage in percents
@property (readonly, nonatomic) float cpuUsage;
@property (readonly, nonatomic) NSString *cpuUsageString;

//An array of dictionaries
@property (readonly, nonatomic) NSArray *runningProcesses;

@end
