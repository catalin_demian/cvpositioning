// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CVPLogEntry.h instead.

#import <CoreData/CoreData.h>

extern const struct CVPLogEntryAttributes {
	__unsafe_unretained NSString *appState;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *message;
	__unsafe_unretained NSString *source;
} CVPLogEntryAttributes;

@interface CVPLogEntryID : NSManagedObjectID {}
@end

@interface _CVPLogEntry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CVPLogEntryID* objectID;

@property (nonatomic, strong) NSString* appState;

//- (BOOL)validateAppState:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* message;

//- (BOOL)validateMessage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* source;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@end

@interface _CVPLogEntry (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAppState;
- (void)setPrimitiveAppState:(NSString*)value;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSString*)primitiveMessage;
- (void)setPrimitiveMessage:(NSString*)value;

- (NSString*)primitiveSource;
- (void)setPrimitiveSource:(NSString*)value;

@end
