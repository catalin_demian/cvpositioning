//
//  GeofencigLocationManager.m
//  Positioning
//
//  Created by Alexandru Tudose on 15/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "CVPGeofencingLocationManager.h"
#import "CVPLocationManager.h"
#import "CVPSession.h"
#import "CVPSession+Alerts.h"
#import <Tapptitude/Tapptitude.h>

@interface CVPGeofencingLocationManager () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, assign, getter=isUpdatingLocation) BOOL updatingLocation;
@end

@implementation CVPGeofencingLocationManager

- (instancetype)init {
    if (self = [super init]) {
        self.triggerMode = CVPLocationTriggerGeofence;
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]) {
            self.locationManager.allowsBackgroundLocationUpdates = YES;
        }
        
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
    }
    return self;
}

- (void)dealloc {
    [self stopRegionMonitoring];
}


CVPGeofencingLocationManager *geofenceInstance = nil;
+ (instancetype)instance {
    if (! geofenceInstance) {
        geofenceInstance = [[self alloc] init];
    }
    return geofenceInstance;
}

- (void)startRegionMonitoring {
    if (![CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
        return;
    }
    
    [self startUpdatingLocation];
}

- (void)stopRegionMonitoring {
    if (![CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
        return;
    }
    for (CLCircularRegion *region in self.locationManager.monitoredRegions) {
        [self.locationManager stopMonitoringForRegion:region];
    }
}


- (void)startRegionMonitoringForLocation:(CLLocation *)location {
    if (location) {
        CLLocationDistance radius = [[CVPSession geofenceRadius] doubleValue];
        NSString *identifier = [[[NSUUID alloc] init] UUIDString];
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:location.coordinate radius:radius identifier:identifier];
        region.notifyOnExit = YES;
        region.notifyOnEntry = NO;
        [self.locationManager startMonitoringForRegion:region];
    }
}

- (void)startUpdatingLocation {
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.activityType = [CVPSession activityType].integerValue;
    self.locationManager.distanceFilter = [CVPSession updateDistance].floatValue;
    [self.locationManager requestAlwaysAuthorization];
    
    if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]) {
        self.locationManager.allowsBackgroundLocationUpdates = YES;
    }
    
    [self.locationManager startUpdatingLocation];
    self.updatingLocation = YES;
}

- (void)stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
    self.updatingLocation = NO;
}




- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    [CVPSession showAlertWithTitle:@"Geofence triggered"];
    
    [self stopRegionMonitoring];
    [self startRegionMonitoring];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (self.isUpdatingLocation) {
        self.updatingLocation = NO;
    }
    self.errorCallback(error);
    NSString *message = [NSString stringWithFormat:@"Geofence locationDidFail %@", [error localizedDescription]];
    [CVPSession showAlertWithTitle:message];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSString *message = [NSString stringWithFormat:@"Geofence monitoringDidFailForRegion %@", [error localizedDescription]];
    [CVPSession showAlertWithTitle:message];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    
    BOOL goodAccuracy = location.horizontalAccuracy > 0.0 && location.horizontalAccuracy < 70; //m
    TTLogExpr(location.horizontalAccuracy);
    
    CVPLocationTrigger trigger = self.triggerMode;
    if (self.isUpdatingLocation) {
        trigger = CVPLocationTriggerContinousMonitoring;
    }
    
    if (self.locationUpdateCallback) {
        self.locationUpdateCallback(locations, trigger);
    }
    
    if (self.isUpdatingLocation) {
        if (goodAccuracy) {
            [self stopUpdatingLocation];
            [self startRegionMonitoringForLocation:location];
        }
    } else {
        [self startUpdatingLocation];
    }
    
    BOOL paused = [[CVPLocationManager continousInstance] isDecreasedMonitoringQualityOn];
    if (paused) {
        [[CVPLocationManager continousInstance] startUpdatingLocation:@"geofence"];
    }
}

@end
