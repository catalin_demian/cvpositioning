//
//  SystemManager+Carrier.h
//  Convoyz
//
//  Created by Andrei Dumitru on 7/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

@interface SystemManager (Carrier)

@property (readonly, nonatomic) NSString *mobileCountryCode;
@property (readonly, nonatomic) NSString *mobileNetworkCode;
@property (readonly, nonatomic) NSString *isoCountryCode;

@end
