//
//  MotionSerializer.h
//  Convoyz
//
//  Created by Andrei Dumitru on 6/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

@interface CVPMotionSerializer : NSObject

+ (NSDictionary *)serializePedometerData:(CMPedometerData *)pedometerData;
+ (NSDictionary *)serializeAltitudeData:(CMAltitudeData *)altitudeData;

@end

@interface CVPMotionSerializer (Motion)

+ (NSDictionary *)serializeAccelerometerData:(CMAccelerometerData *)accelerometerData;
+ (NSDictionary *)serializeGyroData:(CMGyroData *)gyroData;
+ (NSDictionary *)serializeMagnetometerData:(CMMagnetometerData *)magnetometerData;
+ (NSDictionary *)serializeDeviceMotion:(CMDeviceMotion *)deviceMotion;

@end

@interface CVPMotionSerializer (Activity)

+ (NSArray *)serializeActivities:(NSArray *)content;
+ (NSDictionary *)serializeActivity:(CMMotionActivity *)activity;

@end
