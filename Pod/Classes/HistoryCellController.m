//
//  HistoryCellController.m
//  Positioning
//
//  Created by Andrei Dumitru on 10/7/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "HistoryCellController.h"
#import "HistoryCell.h"
#import "CVPConstants.h"
#import <Tapptitude/Tapptitude.h>

@interface HistoryCellController ()

@property (strong, nonatomic, nonnull) NSDateFormatter *dateFormatter;

@end

@implementation HistoryCellController

- (id)init {
    if (self = [super initWithCellNibName:NSStringFromClass([HistoryCell class])
                              contentSize:CGSizeMake(-1, 50.0)]) {
        self.minimumInteritemSpacing = 0.0;
        self.minimumLineSpacing = 0.0;
        self.sectionInset = UIEdgeInsetsZero;
        
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    return self;
}

- (BOOL)acceptsContent:(id)content {
    BOOL acceptsContent = NO;
//    if ([content isKindOfClass:[CVPHarvest class]]) {
//        acceptsContent = ([(CVPHarvest *)content typeValue] == HarvestLocationData);
//    }
    return acceptsContent;
}

- (void)configureCell:(HistoryCell *)cell forContent:(id)harvest indexPath:(NSIndexPath *)indexPath {
//    NSTimeInterval timeInterval = [[harvest.content valueForKey:@"timestamp"] doubleValue] / 1000.0;
//    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
//    CLLocationDegrees latitude = [[harvest.content valueForKey:@"latitude"] doubleValue];
//    CLLocationDegrees longitude = [[harvest.content valueForKey:@"longitude"] doubleValue];
//    
//    cell.titleLabel.text = NSStringFormat(@"<%.6f, %.6f>", latitude, longitude);
//    cell.subtitleLabel.text = [self.dateFormatter stringFromDate:date];
}

@end
