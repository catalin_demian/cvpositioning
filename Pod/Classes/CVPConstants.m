//
//  CVPConstants.m
//  CVPositioning
//
//  Created by Catalin Demian on 2/2/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPConstants.h"

const struct CVPSessionSpace CVPSessionSpace = {
    .applicationKillDate = @"applicationKillDate",
    .showAlerts = @"showAlerts",
    .activityType = @"activityType",
    .accuracy = @"accuracy",
    .updateDistance = @"updateDistance",
    .significantChanges = @"significantChanges",
    .geofenceEnabled = @"geofenceEnabled",
    .geofenceRadius = @"geofenceRadius",
    .deferEnabled = @"deferEnabled",
    .deferDistance = @"deferDistance",
    .deferTimeInterval = @"deferTimeInterval",
    .restartMonitoringTimeInterval = @"restartMonitoringTimeInterval",
    .autoPauseLocationByiOS = @"autoPauseLocationByiOS",
    .continousMonitoring = @"continousMonitoring",
    .decreaseAccuracyEnabled = @"decreaseAccuracyEnabled",
    .checkAccuracyTimeInterval = @"checkAccuracyTimeInterval",
    .monitoringBufferTimeInterval = @"monitoringBufferTimeInterval",
    .isDelayedBackgroundSync = @"isDelayedBackgroundSync",
    .motionProcessorEnabled = @"motionProcessorEnabled",
    .visitsEnabled = @"visitsEnabled",
};

const struct CVPAlertSource CVPAlertSource = {
    .visit = @"Visit",
};