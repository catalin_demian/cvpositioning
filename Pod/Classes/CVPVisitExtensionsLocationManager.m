//
//  CVPVisitExtensionsLocationManager.m
//  Pods
//
//  Created by Catalin Demian on 2/15/16.
//
//

#import "CVPVisitExtensionsLocationManager.h"
#import "CVPSession+Alerts.h"


@interface CVPVisitExtensionsLocationManager ()<CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, assign, getter=isUpdatingLocation) BOOL updatingLocation;

@end

@implementation CVPVisitExtensionsLocationManager

- (instancetype)init {
    if (self = [super init]) {
        self.triggerMode = CVPLocationTriggerVisitExtension;
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]) {
            self.locationManager.allowsBackgroundLocationUpdates = YES;
        }
        
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
    }
    return self;
}

- (void)dealloc {
    [self stopMonitoringVisits];
}


CVPVisitExtensionsLocationManager *visitInstance = nil;
+ (instancetype)instance {
    if (! visitInstance) {
        visitInstance = [[self alloc] init];
    }
    return visitInstance;
}










- (void)startMonitoringVisits {
    [self.locationManager startMonitoringVisits];
}

- (void)stopMonitoringVisits {
    [self.locationManager stopMonitoringVisits];
}




- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (self.isUpdatingLocation) {
        self.updatingLocation = NO;
    }
    
    self.errorCallback(error);
    NSString *message = [NSString stringWithFormat:@"Significant locationDidFail %@", [error localizedDescription]];
    [CVPSession showAlertWithTitle:message];
}


- (void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit {
    CVPLocationTrigger trigger = self.triggerMode;
    
    if (self.visitUpdateCallback) {
        self.visitUpdateCallback(visit, trigger);
    }
}


@end
