//
//  System+CPU.m
//  Convoyz
//
//  Created by Andrei Dumitru on 5/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager+CPU.h"
//#import "Debug.h"
#include <sys/sysctl.h>
#include <sys/types.h>
#include <mach/mach.h>
#include <mach/processor_info.h>
#include <mach/mach_host.h>

@implementation SystemManager (CPU)

- (NSString *)cpuUsageString {
    return NSStringFormat(@"%@%%", @(self.cpuUsage));
}

- (float)cpuUsage {
    kern_return_t kr;
    task_info_data_t tinfo;
    mach_msg_type_number_t task_info_count;
    
    task_info_count = TASK_INFO_MAX;
    kr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)tinfo, &task_info_count);
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    
    task_basic_info_t      basic_info;
    thread_array_t         thread_list;
    mach_msg_type_number_t thread_count;
    
    thread_info_data_t     thinfo;
    mach_msg_type_number_t thread_info_count;
    
    thread_basic_info_t basic_info_th;
    uint32_t stat_thread = 0;
    
    basic_info = (task_basic_info_t)tinfo;
    
    kr = task_threads(mach_task_self(), &thread_list, &thread_count);
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    if (thread_count > 0)
        stat_thread += thread_count;
    
    long tot_sec = 0;
    long tot_usec = 0;
    float tot_cpu = 0;
    int j;
    
    for (j = 0; j < thread_count; j++) {
        thread_info_count = THREAD_INFO_MAX;
        kr = thread_info(thread_list[j], THREAD_BASIC_INFO,
                         (thread_info_t)thinfo, &thread_info_count);
        if (kr != KERN_SUCCESS) {
            return -1;
        }
        
        basic_info_th = (thread_basic_info_t)thinfo;
        
        if (!(basic_info_th->flags & TH_FLAGS_IDLE)) {
            tot_sec = tot_sec + basic_info_th->user_time.seconds + basic_info_th->system_time.seconds;
            tot_usec = tot_usec + basic_info_th->system_time.microseconds + basic_info_th->system_time.microseconds;
            tot_cpu = tot_cpu + basic_info_th->cpu_usage / (float)TH_USAGE_SCALE * 100.0;
        }
        
    }
    
    kr = vm_deallocate(mach_task_self(), (vm_offset_t)thread_list, thread_count * sizeof(thread_t));
    assert(kr == KERN_SUCCESS);
    
    return tot_cpu;
}


- (NSArray *)runningProcesses {
    int mib[4] = {CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0};
    size_t miblen = 4;
    size_t size = 0;
    int st = sysctl(mib, (int)miblen, NULL, &size, NULL, 0);
    
    struct kinfo_proc *process = NULL;
    struct kinfo_proc *newprocess = NULL;
    
    do {
        size += size / 10;
        newprocess = realloc(process, size);
        
        if (!newprocess) {
            if (process) free(process);
            return nil;
        }
        
        process = newprocess;
        st = sysctl(mib, (int)miblen, process, &size, NULL, 0);
        
    } while (st == -1 && errno == ENOMEM);
    
    if (st != 0) {
        return nil;
    }
    
    NSMutableArray *array = nil;
    if (size % sizeof(struct kinfo_proc) == 0) {
        int nprocess = (int)(size / sizeof(struct kinfo_proc));
        
        if (nprocess) {
            array = [[NSMutableArray alloc] init];
            for (int i = nprocess - 1; i >= 0; i--) {
                NSDictionary *dict = [self detailsForProcess:process[i]];
                if (dict) {
                    [array addObject:dict];
                }
            }
            free(process);
        }
    }
    return array;
}

#pragma mark - Helpers

- (NSDictionary *)detailsForProcess:(struct kinfo_proc)process {
    NSString *processID = [NSString stringWithFormat:@"%d", process.kp_proc.p_pid];
    NSString *processName = [NSString stringWithFormat:@"%s", process.kp_proc.p_comm];
    NSString *processPriority = [NSString stringWithFormat:@"%d", process.kp_proc.p_priority];
    NSDate *processStartDate = [NSDate dateWithTimeIntervalSince1970:process.kp_proc.p_un.__p_starttime.tv_sec];
    NSString *processParentID = [NSString stringWithFormat:@"%d", [self parentPIDForProcess:(int)process.kp_proc.p_pid]];
    NSString *processStatus = [NSString stringWithFormat:@"%d", (int)process.kp_proc.p_stat];
    NSString *processFlags = [NSString stringWithFormat:@"%d", (int)process.kp_proc.p_flag];

    if (processID == nil || processID.length <= 0) {
        processID = @"Unkown";
    }
    if (processName == nil || processName.length <= 0) {
        processName = @"Unkown";
    }
    if (processPriority == nil || processPriority.length <= 0) {
        processPriority = @"Unkown";
    }
    if (processStartDate == nil) {
        processStartDate = [NSDate date];
    }
    if (processParentID == nil || processParentID.length <= 0) {
        processParentID = @"Unkown";
    }
    if (processStatus == nil || processStatus.length <= 0) {
        processStatus = @"Unkown";
    }
    if (processFlags == nil || processFlags.length <= 0) {
        processFlags = @"Unkown";
    }
    
    NSArray *itemArray = @[processID, processName, processPriority, processStartDate, processParentID, processStatus, processFlags];
    NSArray *keyArray = @[@"PID", @"Name", @"Priority", @"StartDate", @"ParentID", @"Status", @"Flags"];
    
    return [[NSDictionary alloc] initWithObjects:itemArray forKeys:keyArray];
}

- (int)parentPIDForProcess:(int)pid {
    struct kinfo_proc info;
    size_t length = sizeof(struct kinfo_proc);
    int mib[4] = { CTL_KERN, KERN_PROC, KERN_PROC_PID, pid };
    
    if (sysctl(mib, 4, &info, &length, NULL, 0) < 0) {
        return -1;
    }
    
    if (length == 0) {
        return -1;
    }
    int PPID = info.kp_eproc.e_ppid;
    if (PPID <= 0) {
        return -1;
    }
    return PPID;
}

@end
