//
//  CVPositioning+Screens.h
//  CVPositioning
//
//  Created by Catalin Demian on 2/4/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPositioning.h"

@interface CVPositioning (Screens)

+ (UIViewController *)settingsViewController;
+ (UIViewController *)historyViewController;
+ (UIViewController *)motionViewController;
+ (UIViewController *)visitsViewController;

+ (UIViewController *)menuViewController;

@end
