//
//  ViewController.h
//  Positioning
//
//  Created by Andrei Dumitru on 10/5/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *activityTypeTextField;
@property (weak, nonatomic) IBOutlet UITextField *accuracyTextField;
@property (weak, nonatomic) IBOutlet UITextField *distanceTextField;
@property (weak, nonatomic) IBOutlet UITextField *geofenceRadiusTextField;
@property (weak, nonatomic) IBOutlet UITextField *deferDistanceTextField;
@property (weak, nonatomic) IBOutlet UITextField *deferIntervalTextField;
@property (weak, nonatomic) IBOutlet UITextField *restartTimerTextField;
@property (weak, nonatomic) IBOutlet UITextField *checkAccuracyTimerTextField;

@property (weak, nonatomic) IBOutlet UISwitch *alertsSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *significatChangesSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *geofenceSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *deferSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *autoPauseByiOSSwtich;
@property (weak, nonatomic) IBOutlet UISwitch *continousMonitoringSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *decreaseAccuracySwitch;
@property (weak, nonatomic) IBOutlet UITextField *monitoringBufferTimeTextField;
@property (weak, nonatomic) IBOutlet UISwitch *delayedBackgroundSyncSwtich;
@property (weak, nonatomic) IBOutlet UISwitch *motionProcessorSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *visitsSwitch;

@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *geofenceLabels;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *geofenceTextFields;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *deferLabels;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *deferTextFields;

- (IBAction)decreaseMonitoringAcurracy:(id)sender;
- (IBAction)delayedBackgroundSyncAction:(id)sender;
- (IBAction)motionProcessorAction:(id)sender;

@end

