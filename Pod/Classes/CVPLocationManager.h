//
//  LocationManager.h
//  Positioning
//
//  Created by Andrei Dumitru on 10/6/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CVPConstants.h"

@interface CVPLocationManager : NSObject

@property (readonly, nonatomic) BOOL isAuthorized;
@property (readonly, nonatomic) BOOL locationServicesEnabled;
@property (assign, nonatomic, getter=isUpdatingLocation) BOOL updatingLocation;
@property (assign, nonatomic) BOOL isDecreasedMonitoringQualityOn;

@property (readwrite, nonatomic) CLLocationDistance distanceFilter;
@property (readwrite, nonatomic) CLActivityType activityType;
@property (readwrite, nonatomic) CVPLocationTrigger triggerMode;
- (void)setLocationAccuracy:(CLLocationAccuracy)accuracy;

@property (copy, nonatomic) void (^locationUpdateCallback)(NSArray *locations, CVPLocationTrigger trigger);
@property (copy, nonatomic) void (^errorCallback)(NSError *error);

//+ (instancetype)instance;

+ (instancetype)continousInstance;

- (instancetype)initForContinousMonitoring;

- (void)startUpdatingLocation:(NSString *)reason;
- (void)stopUpdatingLocation;

- (void)allowDeferredLocationUpdatesUntilTraveled:(CLLocationDistance)distance timeout:(NSTimeInterval)timeout;
- (void)disallowDeferredLocationUpdates;

- (void)setAutoPauseByiOS:(BOOL)autoPause;

- (void)decreaseMonitoringQualityWithTrigger:(NSString *)trigger;

@end


extern CVPLocationManager *continousInstance;
