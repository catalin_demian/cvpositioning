//
//  LogEntryCellController.m
//  Positioning
//
//  Created by Alexandru Tudose on 12/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "LogEntryCellController.h"
#import "CVPLogEntry.h"
#import "HistoryCell.h"

@interface LogEntryCellController ()

@property (strong, nonatomic, nonnull) NSDateFormatter *dateFormatter;

@end

@implementation LogEntryCellController

- (id)init {
    if (self = [super initWithCellNibName:NSStringFromClass([HistoryCell class])
                              contentSize:CGSizeMake(-1, 50.0)]) {
        self.minimumInteritemSpacing = 0.0;
        self.minimumLineSpacing = 0.0;
        self.sectionInset = UIEdgeInsetsZero;
        
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    return self;
}

- (BOOL)acceptsContent:(id)content {
    return [content isKindOfClass:[CVPLogEntry class]];
}

- (void)configureCell:(HistoryCell *)cell forContent:(CVPLogEntry *)log indexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = log.message;
    cell.subtitleLabel.text = [self.dateFormatter stringFromDate:log.date];
    
    // entry color
    cell.rightView.hidden = [log.appState isEqual:@"active"];
    if (! cell.rightView.hidden) {
        if ([log.appState isEqual:@"background"]) {
            cell.rightView.backgroundColor = [UIColor orangeColor];
        } else if ([log.appState isEqual:@"inactive"]){
            cell.rightView.backgroundColor = [UIColor purpleColor];
        } else {
            cell.rightView.hidden = YES;
        }
    }
    
    if ([log.message hasPrefix:@"geofence"]) {
        cell.backgroundColor = [UIColor colorWithWhite:.95 alpha:1.0];
    } else if ([log.message hasPrefix:@"significant"]) {
        cell.backgroundColor = [UIColor colorWithWhite:.85 alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
}

@end