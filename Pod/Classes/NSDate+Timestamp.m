//
//  NSDate+Timestamp.m
//  Convoyz
//
//  Created by Andrei Dumitru on 7/1/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "NSDate+Timestamp.h"

@implementation NSDate (Timestamp)

- (NSTimeInterval)timestamp {
    return floor([self timeIntervalSince1970]);
}

@end
