//
//  CVPVisitHistoryViewController.h
//  Pods
//
//  Created by Catalin Demian on 2/17/16.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Tapptitude/TTCollectionFeedViewController.h>

@interface CVPVisitHistoryViewController : TTCollectionFeedViewController<MKMapViewDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@end
