//
//  NSError+CoreLocation.h
//  Positioning
//
//  Created by Alexandru Tudose on 19/01/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (CoreLocation)

- (NSString *)failedReason;

@end
