// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CVPLogEntry.m instead.

#import "_CVPLogEntry.h"

const struct CVPLogEntryAttributes CVPLogEntryAttributes = {
	.appState = @"appState",
	.date = @"date",
	.message = @"message",
	.source = @"source",
};

@implementation CVPLogEntryID
@end

@implementation _CVPLogEntry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LogEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LogEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LogEntry" inManagedObjectContext:moc_];
}

- (CVPLogEntryID*)objectID {
	return (CVPLogEntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic appState;

@dynamic date;

@dynamic message;

@dynamic source;

@end

