//
//  CVPVisitExtensionsLocationManager.h
//  Pods
//
//  Created by Catalin Demian on 2/15/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocationManager+CLVisitExtensions.h>
#import "CVPConstants.h"

@interface CVPVisitExtensionsLocationManager : NSObject

@property (readwrite, nonatomic) CVPLocationTrigger triggerMode;

@property (copy, nonatomic) void (^visitUpdateCallback)(CLVisit *visit, CVPLocationTrigger trigger);
@property (copy, nonatomic) void (^errorCallback)(NSError *error);

+ (instancetype)instance;

- (void)startMonitoringVisits;
- (void)stopMonitoringVisits;

extern CVPVisitExtensionsLocationManager *visitInstance;

@end
