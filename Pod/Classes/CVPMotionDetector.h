//
//  MotionDetector.h
//  Positioning
//
//  Created by Alexandru Tudose on 16/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

@interface CVPMotionDetector : NSObject

@property (strong, nonatomic) CMMotionActivityManager *motionActivityManager;
@property (nonatomic, copy) CMMotionActivityHandler callback;

@property (nonatomic, copy) void (^motionDetectedCallback)(NSString *reason);

+ (instancetype)instance;

- (void)didMovedInLastHour:(void(^)(BOOL didMove))callback;

- (void)triggerAuthorizationCheck;

- (void)fetchHistoryActivities:(void(^)(NSArray *activities, NSError *error))callback;

@end


extern CVPMotionDetector *motionDetector;