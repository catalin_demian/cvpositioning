//
//  ZOHeaderView.h
//  Zonga
//
//  Created by Andrei Dumitru on 4/13/14.
//  Copyright (c) 2014 Trilulilu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface HeaderView : UICollectionReusableView

@property (nonatomic, weak) IBOutlet UILabel *headerLabel;

@end
