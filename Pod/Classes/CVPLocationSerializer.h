//
//  LocationSerializer.h
//  Convoyz
//
//  Created by Andrei Dumitru on 6/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CVPLocationSerializer : NSObject

+ (NSArray *)serializeLocations:(NSArray *)content;
+ (NSDictionary *)serializeLocation:(CLLocation *)location;

+ (NSDictionary *)serializeHeading:(CLHeading *)heading;

@end
