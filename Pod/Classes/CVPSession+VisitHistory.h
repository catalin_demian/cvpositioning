//
//  CVPSession+VisitHistory.h
//  Pods
//
//  Created by Catalin Demian on 2/17/16.
//
//

#import <CVPSession.h>
#import <CVPVisit.h>

@interface CVPSession (VisitHistory)

+ (void)saveVisit:(CLVisit *)visit withCallback:(void(^)(CVPVisit *visit, NSError *error))callback;

+ (void)deleteOldVisitsWithCallback:(void(^)(NSError *error))callback;

+ (NSArray *)fetchRecentVisits;

@end
