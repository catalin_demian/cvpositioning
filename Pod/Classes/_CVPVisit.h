// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CVPVisit.h instead.

@import CoreData;

extern const struct CVPVisitAttributes {
	__unsafe_unretained NSString *accuracy;
	__unsafe_unretained NSString *arrivalDate;
	__unsafe_unretained NSString *dateTime;
	__unsafe_unretained NSString *departureDate;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longitude;
} CVPVisitAttributes;

@interface CVPVisitID : NSManagedObjectID {}
@end

@interface _CVPVisit : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CVPVisitID* objectID;

@property (nonatomic, strong) NSNumber* accuracy;

@property (atomic) double accuracyValue;
- (double)accuracyValue;
- (void)setAccuracyValue:(double)value_;

//- (BOOL)validateAccuracy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* arrivalDate;

//- (BOOL)validateArrivalDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* dateTime;

//- (BOOL)validateDateTime:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* departureDate;

//- (BOOL)validateDepartureDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* latitude;

@property (atomic) double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longitude;

@property (atomic) double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@end

@interface _CVPVisit (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAccuracy;
- (void)setPrimitiveAccuracy:(NSNumber*)value;

- (double)primitiveAccuracyValue;
- (void)setPrimitiveAccuracyValue:(double)value_;

- (NSDate*)primitiveArrivalDate;
- (void)setPrimitiveArrivalDate:(NSDate*)value;

- (NSDate*)primitiveDateTime;
- (void)setPrimitiveDateTime:(NSDate*)value;

- (NSDate*)primitiveDepartureDate;
- (void)setPrimitiveDepartureDate:(NSDate*)value;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;

@end
