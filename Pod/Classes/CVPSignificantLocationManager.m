//
//  SignificantLocationManager.m
//  Positioning
//
//  Created by Alexandru Tudose on 15/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "CVPSignificantLocationManager.h"
#import "CVPSession+Alerts.h"

@interface CVPSignificantLocationManager ()<CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, assign, getter=isUpdatingLocation) BOOL updatingLocation;

@end

@implementation CVPSignificantLocationManager

- (instancetype)init {
    if (self = [super init]) {
        self.triggerMode = CVPLocationTriggerSignificantChanges;
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]) {
            self.locationManager.allowsBackgroundLocationUpdates = YES;
        }
        
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
    }
    return self;
}

- (void)dealloc {
    [self stopMonitoringSignificantLocationChanges];
}


CVPSignificantLocationManager *significantInstance = nil;
+ (instancetype)instance {
    if (! significantInstance) {
        significantInstance = [[self alloc] init];
    }
    return significantInstance;
}












- (void)startMonitoringSignificantLocationChanges {
    if (![CLLocationManager significantLocationChangeMonitoringAvailable]) {
        return;
    }
    [self.locationManager startMonitoringSignificantLocationChanges];
}

- (void)stopMonitoringSignificantLocationChanges {
    if (![CLLocationManager significantLocationChangeMonitoringAvailable]) {
        return;
    }
    [self.locationManager stopMonitoringSignificantLocationChanges];
}




- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (self.isUpdatingLocation) {
        self.updatingLocation = NO;
    }
    
    self.errorCallback(error);
    NSString *message = [NSString stringWithFormat:@"Significant locationDidFail %@", [error localizedDescription]];
    [CVPSession showAlertWithTitle:message];
}





- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    
    BOOL goodAccuracy = location.horizontalAccuracy > 0.0 && location.horizontalAccuracy < 10; //m
    
    CVPLocationTrigger trigger = self.triggerMode;
    
    if (self.locationUpdateCallback) {
        self.locationUpdateCallback(locations, trigger);
    }
    
    BOOL paused = [[CVPLocationManager continousInstance] isDecreasedMonitoringQualityOn];
    if (paused) {
        [[CVPLocationManager continousInstance] startUpdatingLocation:@"significant change"];
    }
}

@end
