//
//  HistoryViewController.h
//  Positioning
//
//  Created by Andrei Dumitru on 10/7/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <Tapptitude/Tapptitude.h>

@interface HistoryViewController : TTCollectionFeedViewController

@property (nonatomic, assign) BOOL showOnlyVisits;

@end
