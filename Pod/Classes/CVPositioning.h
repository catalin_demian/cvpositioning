//
//  CVPPositioningManager.h
//  CVPositioning
//
//  Created by Catalin Demian on 2/3/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CVPConstants.h"

@protocol CVPositioningObserver <NSObject>

- (void)didReceiveLocationUpdates:(NSArray<CLLocation * > *)locationUpdates withTrigger:(CVPLocationTrigger)trigger;
- (void)didReceiveError:(NSError *)error;

@end

@interface CVPositioning : NSObject

+ (instancetype)globalInstance;

- (CVPLocationTrigger)triggerForLastKnownLocation;
- (CLLocation *)lastKnownLocation;
- (NSDate *)dateForLastKnownLocation;
- (void)getCurrentLocationWithCallback:(void(^)(CLLocation *location, NSError *error))callback;

- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;

- (void)registerObserverForPositionUpdates:(id)observer;
- (void)unregisterObserver:(id)observer;

@end

