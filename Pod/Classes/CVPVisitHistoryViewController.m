//
//  CVPVisitHistoryViewController.m
//  Pods
//
//  Created by Catalin Demian on 2/17/16.
//
//

#import "CVPVisitHistoryViewController.h"
#import "CVPSession+VisitHistory.h"
#import "HeaderView.h"
#import <Tapptitude/Tapptitude.h>
#import <Tapptitude/TTFetchedDataSource.h>
#import "CVPVisitCellController.h"
#import "CVPVisitCell.h"

@interface CVPVisitHistoryViewController () <MKMapViewDelegate>

@property (nonatomic, strong) NSArray *visitArray;
@property (nonatomic, strong) NSTimer *reloadTimer;
@property (nonatomic, strong) CVPVisit *selectedVisit;

@end

@implementation CVPVisitHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Locations", nil);
    
    [self registerCollectionHeaderView];
    UICollectionViewFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    layout.sectionHeadersPinToVisibleBounds = YES;
    
    CVPVisitCellController *cellController = [[CVPVisitCellController alloc] init];
    @weakify(self);
    [cellController setDidSelectContentBlock:^(id content, NSIndexPath *indexPath) {
        @strongify(self);
        [self.mapView showAnnotations:@[content] animated:YES];
        self.selectedVisit = content;
        [self.collectionView reloadData];
    }];
    [cellController setConfigureCellBlock:^(CVPVisitCell *cell, CVPVisit *content, NSIndexPath *indexPath) {
        @strongify(self);
        cell.backgroundColor = content == self.selectedVisit ? [UIColor lightGrayColor] : [UIColor whiteColor];
        cell.titleLabel.text = [content visitDescription];
        cell.subtitleLabel.text = [content dateTimeString];
    }];
    
    self.cellController = cellController;
    
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self initializeTimer];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.reloadTimer invalidate];
    self.reloadTimer = nil;
}

- (void)dealloc {
    [self.reloadTimer invalidate];
    self.reloadTimer = nil;
}

- (void)initializeTimer {
    self.reloadTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(reloadData) userInfo:nil repeats:YES];
}


- (void)reloadData {
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
    NSFetchRequest *request = [CVPVisit MR_requestAllSortedBy:@"dateTime" ascending:NO withPredicate:nil inContext:context];
    self.dataSource = [[TTFetchedDataSource alloc] initWithFetchRequest:request sectionKeyPath:@"sectionTitle" context:context];
}

- (void)addAnnotationsForVisitArray:(NSArray *)visitArray {
    [self.mapView addAnnotations:visitArray];
    [self.mapView showAnnotations:visitArray animated:self.view.window != nil];
}

#pragma mark - Collection View

- (void)registerCollectionHeaderView {
    NSString *nibName = NSStringFromClass([HeaderView class]);
    UINib *headerNib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:nibName];
    
    UIView *headerView = [[headerNib instantiateWithOwner:nil options:nil] lastObject];
    UICollectionViewFlowLayout *flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.headerReferenceSize = headerView.bounds.size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        CVPVisit *entry = [self.dataSource objectAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:indexPath.section]];
        
        NSString *nibName = NSStringFromClass([HeaderView class]);
        HeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:nibName forIndexPath:indexPath];
        headerView.headerLabel.text = entry.sectionTitle;
        return headerView;
    } else {
        return [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
}

#pragma mark - DataSource

- (void)dataSourceDidReloadContent:(TTFetchedDataSource *)dataSource {
    [super dataSourceDidReloadContent:dataSource];
    NSArray *flattenedArray = [dataSource.content valueForKeyPath:@"@unionOfArrays.self"];
    BOOL animate = NO;
    if (!self.mapView.annotations.count) {
        animate = YES;
    }
    [self.mapView addAnnotations:flattenedArray];
    if (animate) {
        [self.mapView showAnnotations:flattenedArray animated:self.view.window];
    }
}

#pragma mark - MapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    self.selectedVisit = view.annotation;
    [self reloadData];
    [self scrollToContent:self.selectedVisit animated:YES];
}



@end
