#import "CVPVisit.h"

@interface CVPVisit ()

// Private interface goes here.

@end

@implementation CVPVisit

- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake(self.latitudeValue, self.longitudeValue);
}

- (NSString *)sectionTitle {
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = [NSString stringWithFormat:@"dd MMMM"];
    });
    
    return [dateFormatter stringFromDate:self.dateTime];
    
}

- (NSString *)visitDescription {
    static NSDateFormatter *dateFormatter = nil;
    if (! dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    NSString *arrival = [self.arrivalDate isEqualToDate:[NSDate distantPast]] ? @"Past" : [dateFormatter stringFromDate:self.arrivalDate];
    NSString *departure = [self.departureDate isEqualToDate:[NSDate distantFuture]] ? @"Future" : [dateFormatter stringFromDate:self.departureDate];
    
    NSString *source = @"Visit ";
    NSString *locationInfo = NSStringFormat(@"<%.6f, %.6f>", self.coordinate.latitude, self.coordinate.longitude);
    NSString *title = NSStringFormat(@"%@%@, [%@ - %@]", source, locationInfo, arrival, departure);
    return title;
}

- (NSString *)dateTimeString {
    static NSDateFormatter *dateFormatter = nil;
    if (! dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    return [dateFormatter stringFromDate:self.dateTime];
}

@end
