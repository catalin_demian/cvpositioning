#import "CVPLogEntry.h"

@interface CVPLogEntry ()

// Private interface goes here.

@end

@implementation CVPLogEntry

- (NSString *)sectionTitle {
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = [NSString stringWithFormat:@"dd MMMM"];
    });
    
    return [dateFormatter stringFromDate:self.date];
    
}

@end
