//
//  System+Disk.h
//  Convoyz
//
//  Created by Andrei Dumitru on 5/20/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

@interface SystemManager (Disk)

//Total disk space in number of bytes
@property (readonly, nonatomic) NSNumber *diskSpace;

//Free disk space in number of bytes
@property (readonly, nonatomic) NSNumber *freeDiskSpace;

//Used disk space in number of bytes
@property (readonly, nonatomic) NSNumber *usedDiskSpace;

//Total disk space as a string formatted xx.xx GB or xx.xx MB
- (NSString *)diskSpaceString;

//Free disk space as a string formatted xx.xx GB or xx.xx MB of in percent
- (NSString *)freeDiskSpaceString:(BOOL)inPercent;

//Used disk space as a string formatted xx.xx GB or xx.xx MB of in percent
- (NSString *)usedDiskSpaceString:(BOOL)inPercent;

@end
