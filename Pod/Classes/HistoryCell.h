//
//  HistoryCell.h
//  Positioning
//
//  Created by Andrei Dumitru on 10/7/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@end
