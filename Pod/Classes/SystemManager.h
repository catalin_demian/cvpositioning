//
//  System.h
//  Convoyz
//
//  Created by Andrei Dumitru on 5/20/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SystemManager : NSObject

@property (readonly, nonatomic) BOOL proximityState;
@property (readonly, nonatomic) float batteryLevel;
@property (readonly, nonatomic) CGFloat screenBrightness;

@property (readonly, nonatomic) UIApplicationState applicationState;
@property (readonly, nonatomic) NSString *applicationStateString;

@property (readonly, nonatomic) NSDictionary *stateDictionary;

+ (instancetype)instance;

@end
