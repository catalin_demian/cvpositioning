// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CVPVisit.m instead.

#import "_CVPVisit.h"

const struct CVPVisitAttributes CVPVisitAttributes = {
	.accuracy = @"accuracy",
	.arrivalDate = @"arrivalDate",
	.dateTime = @"dateTime",
	.departureDate = @"departureDate",
	.latitude = @"latitude",
	.longitude = @"longitude",
};

@implementation CVPVisitID
@end

@implementation _CVPVisit

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Visit" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Visit";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Visit" inManagedObjectContext:moc_];
}

- (CVPVisitID*)objectID {
	return (CVPVisitID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"accuracyValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"accuracy"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic accuracy;

- (double)accuracyValue {
	NSNumber *result = [self accuracy];
	return [result doubleValue];
}

- (void)setAccuracyValue:(double)value_ {
	[self setAccuracy:@(value_)];
}

- (double)primitiveAccuracyValue {
	NSNumber *result = [self primitiveAccuracy];
	return [result doubleValue];
}

- (void)setPrimitiveAccuracyValue:(double)value_ {
	[self setPrimitiveAccuracy:@(value_)];
}

@dynamic arrivalDate;

@dynamic dateTime;

@dynamic departureDate;

@dynamic latitude;

- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:@(value_)];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:@(value_)];
}

@dynamic longitude;

- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:@(value_)];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:@(value_)];
}

@end

