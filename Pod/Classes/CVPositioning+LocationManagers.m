//
//  CVPositioning+LocationManagers.m
//  CVPositioning
//
//  Created by Catalin Demian on 2/4/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPositioning+LocationManagers.h"
#import "CVPGeofencingLocationManager.h"
#import "CVPSignificantLocationManager.h"
#import "CVPVisitExtensionsLocationManager.h"
#import "CVPLocationManager.h"
#import "CVPSession+Alerts.h"
#import "CVPSession+VisitHistory.h"

@interface CVPositioning ()

- (void)notifyObserversOfError:(NSError *)error;
- (void)notifyObserversOfLocationUpdates:(NSArray *)locations withTrigger:(CVPLocationTrigger)trigger;

@end

@implementation CVPositioning (LocationManagers)

- (void)releaseLocationManagerForTrigger:(CVPLocationTrigger)trigger {
    switch (trigger) {
        case CVPLocationTriggerContinousMonitoring:
            [continousInstance setLocationUpdateCallback:nil];
            continousInstance = nil;
            break;
        case CVPLocationTriggerSignificantChanges:
            [significantInstance setLocationUpdateCallback:nil];
            significantInstance = nil;
            break;
        case CVPLocationTriggerGeofence:
            [geofenceInstance setLocationUpdateCallback:nil];
            geofenceInstance = nil;
            break;
        case CVPLocationTriggerVisitExtension:
            [visitInstance setVisitUpdateCallback:nil];
            visitInstance = nil;
        default:
            break;
    }
}

- (CVPLocationManager *)locationManagerForTrigger:(CVPLocationTrigger)trigger {
    CVPLocationManager *manager = nil;
    switch (trigger) {
        case CVPLocationTriggerContinousMonitoring:
            manager = [[CVPLocationManager alloc] initForContinousMonitoring];
            continousInstance = manager;
            break;
        case CVPLocationTriggerSignificantChanges:
            manager = [CVPSignificantLocationManager instance];
            significantInstance = manager;
            break;
        case CVPLocationTriggerGeofence:
            manager = [CVPGeofencingLocationManager instance];
            geofenceInstance = manager;
            break;
        case CVPLocationTriggerVisitExtension:
            manager = [CVPVisitExtensionsLocationManager instance];
            visitInstance = manager;
            break;
        default:
            break;
    }
    
    @weakify(self);
    [manager setErrorCallback:^(NSError *error) {
        @strongify(self);
        [self notifyObserversOfError:error];
    }];
    
    if (trigger == CVPLocationTriggerVisitExtension) {
        [(CVPVisitExtensionsLocationManager *)manager setVisitUpdateCallback:^(CLVisit *visit, CVPLocationTrigger trigger) {
            [CVPSession deleteOldVisitsWithCallback:nil];
            [CVPSession saveVisit:visit withCallback:nil];
            [CVPSession showAlertForVisit:visit];
        }];
        return manager;
    }
    
    [manager setLocationUpdateCallback:^(NSArray *locations, CVPLocationTrigger trigger) {
        @strongify(self);
        for (CLLocation *location in locations) {
            [CVPSession showAlertForLocation:location trigger:trigger];
        }
        [self performSelector:@selector(postLocationUpdateNotification) withObject:nil afterDelay:0.1f];
        [self notifyObserversOfLocationUpdates:locations withTrigger:trigger];
    }];
    

    
    return manager;
}

- (void)postLocationUpdateNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:CVPLocationDidUpdateNotification object:nil];
}

@end
