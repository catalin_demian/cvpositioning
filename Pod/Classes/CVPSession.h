//
//  Session.h
//  Convoyz
//
//  Created by Alexandru Tudose on 16.02.2015.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CVPConstants.h"

@interface CVPSession : NSObject

+ (BOOL)showAlerts;
+ (NSDate *)applicationKillDate;
+ (NSNumber *)activityType;
+ (NSNumber *)updateDistance;
+ (BOOL)significantChanges;
+ (BOOL)geofenceEnabled;
+ (NSNumber *)geofenceRadius;
+ (BOOL)deferEnabled;
+ (NSNumber *)deferDistance;
+ (NSNumber *)deferTimeInterval;
+ (NSNumber *)restartMonitoringTimeInterval;
+ (BOOL)autoPauseLocationByiOSEnabled;
+ (BOOL)continousMonitoringEnabled;
+ (BOOL)decreaseAccuracyEnabled;
+ (NSNumber *)checkAccuracyTimeInterval;
+ (NSNumber *)monitoringBufferInterval;
+ (BOOL)isDelayedBackgroundSync;
+ (NSNumber *)locationAccuracy;
+ (BOOL)motionProcessorEnabled;
+ (BOOL)visitsEnabled;


+ (void)setUpdateDistance:(NSNumber *)updateDistance;
+ (void)setShowAlerts:(BOOL)showAlerts;
+ (void)setApplicationKillDate:(NSDate *)date;
+ (void)setActivityType:(NSNumber *)activityType;
+ (void)setSignificantChanges:(BOOL)significantChanges;
+ (void)setGeofenceEnabled:(BOOL)geofencesEnabled;
+ (void)setVisitsEnabled:(BOOL)visitsEnabled;
+ (void)setGeofenceRadius:(NSNumber *)geofenceRadius;
+ (void)setDeferEnabled:(BOOL)deferEnabled;
+ (void)setDeferDistance:(NSNumber *)deferDistance;
+ (void)setDeferTimeInterval:(NSNumber *)deferTimeInterval;
+ (void)setRestartMonitoringTimeInterval:(NSNumber *)restartMonitoringTimeInterval;
+ (void)setAutoPauseLocationByiOSEnabled:(BOOL)autoPause;
+ (void)setContinousMonitoringEnabled:(BOOL)contiousEnabled;
+ (void)setDecreaseAccuracyEnabled:(BOOL)decreaseAccuracyEnabled;
+ (void)setCheckAccuracyTimeInterval:(NSNumber *)checkAccuracyTimeInterval;
+ (void)setMonitoringBufferTimeInterval:(NSNumber *)monitoringBufferTimeInterval;
+ (void)setIsDelayedBackgroundSync:(BOOL)isDelayed;
+ (void)setLocationAccuracy:(NSNumber *)accuracy;
+ (void)setMotionProcessorEnabled:(BOOL)enabled;

@end
