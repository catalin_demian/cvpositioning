//
//  ViewController.m
//  Positioning
//
//  Created by Andrei Dumitru on 10/5/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "SettingsViewController.h"
#import "CVPSession.h"
#import "CVPLocationManager.h"
#import <Tapptitude/UIView+FirstResponder.h>
#import <Tapptitude/TTTouchRecognizer.h>
#import <Tapptitude/Tapptitude.h>
#import "CVPSignificantLocationManager.h"
#import "CVPGeofencingLocationManager.h"
#import "CVPVisitExtensionsLocationManager.h"
#import "AccuracyPickerDataSource.h"
#import "CVPMotionDetector.h"
#import "CVPositioning.h"

@interface SettingsViewController () <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) TTTouchRecognizer *tapGestureRecognizer;
@property (strong, nonatomic) NSArray *activityTypes;
@property (nonatomic, strong) AccuracyPickerDataSource *accuracyDataSource;

@end

@implementation SettingsViewController

- (instancetype)init {
    if (self = [super init]) {
        self.title  = NSLocalizedString(@"Settings", nil);
        self.activityTypes = @[@(CLActivityTypeOther), @(CLActivityTypeAutomotiveNavigation), @(CLActivityTypeFitness), @(CLActivityTypeOtherNavigation)];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    NSArray *barButtonItems = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelEditingAction:)],
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditingAction:)]];
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarTintColor:[UIColor whiteColor]];
    [toolbar setItems:barButtonItems];
    [toolbar sizeToFit];
    
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    self.accuracyDataSource = [[AccuracyPickerDataSource alloc] init];
    UIPickerView *accPickerView = [[UIPickerView alloc] init];
    accPickerView.backgroundColor = [UIColor whiteColor];
    accPickerView.delegate = self.accuracyDataSource;
    accPickerView.dataSource = self.accuracyDataSource;
    
    self.activityTypeTextField.inputAccessoryView = toolbar;
    self.activityTypeTextField.inputView = pickerView;
    self.accuracyTextField.inputAccessoryView = toolbar;
    self.accuracyTextField.inputView = accPickerView;
    self.distanceTextField.inputAccessoryView = toolbar;
    self.geofenceRadiusTextField.inputAccessoryView = toolbar;
    self.deferDistanceTextField.inputAccessoryView = toolbar;
    self.deferIntervalTextField.inputAccessoryView = toolbar;
    self.restartTimerTextField.inputAccessoryView = toolbar;
    self.monitoringBufferTimeTextField.inputAccessoryView = toolbar;
    [self reload];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    CGSize size = CGSizeMake(self.scrollView.bounds.size.width, CGRectGetMaxY(self.logoutButton.frame) + 0.5 * CGRectGetHeight(self.logoutButton.frame));
    
    self.scrollView.contentSize = size;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger count = 0;
    if (component == 0) {
        count = self.activityTypes.count;
    }
    return count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component != 0) {
        return nil;
    }
    return [self activityTypeString:self.activityTypes[row]];
}

#pragma mark - Notifications

- (void)keyboardWillShowNotification:(NSNotification *)notification {
    CGSize keyboardSize = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    keyboardSize.height -= self.tabBarController.tabBar.bounds.size.height;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    CGRect frame = [self.view findFirstResponder].frame;
    
    CGRect aRect = self.view.bounds;
    aRect.size.height -= keyboardSize.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        if (!CGRectContainsPoint(aRect, frame.origin) && !CGRectEqualToRect(CGRectZero, frame)) {
            CGPoint offset = CGPointMake(0.0, CGRectGetMaxY(frame) + keyboardSize.height - self.view.bounds.size.height);
            [self.scrollView setContentOffset:offset animated:NO];
        }
    }];
}

- (void)keyboardWillHideNotification:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.contentInset = UIEdgeInsetsZero;
        self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}

#pragma mark - Actions

- (IBAction)doneEditingAction:(id)sender {
    UITextField *textField = (UITextField *)[self.view findFirstResponder];
    double value = textField.text.doubleValue;
    
    if (textField == self.activityTypeTextField) {
        UIPickerView *pickerView = (UIPickerView *)textField.inputView;
        NSInteger index = [pickerView selectedRowInComponent:0];
        CLActivityType activityType = [self.activityTypes[index] integerValue];
        [CVPSession setActivityType:@(activityType)];
        [[CVPLocationManager continousInstance] setActivityType:activityType];
    } else if (textField == self.accuracyTextField) {
        UIPickerView *pickerView = (UIPickerView *)textField.inputView;
        NSInteger index = [pickerView selectedRowInComponent:0];
        NSString *accuracyName = [self.accuracyDataSource nameForIndex:index];
        textField.text = accuracyName;
        CLLocationAccuracy accuracy = [self.accuracyDataSource accuracyForName:accuracyName];
        [CVPSession setLocationAccuracy:@(accuracy)];
        [[CVPLocationManager continousInstance] setLocationAccuracy:accuracy];
    } else if (textField == self.distanceTextField) {
        [[CVPLocationManager continousInstance] setDistanceFilter:value];
        [CVPSession setUpdateDistance:@(value)];
    } else if (textField == self.geofenceRadiusTextField) {
        [CVPSession setGeofenceRadius:@(value)];
    } else if (textField == self.deferDistanceTextField) {
        [CVPSession setDeferDistance:@(value)];
    } else if (textField == self.deferIntervalTextField) {
        value = value * CVPMinuteTimeInterval;
        [CVPSession setDeferTimeInterval:@(value)];
    } else if (textField == self.restartTimerTextField) {
        if (value < 10 && value >= 1) {
            textField.text = [CVPSession restartMonitoringTimeInterval] ? [[CVPSession restartMonitoringTimeInterval] stringValue] : @"";
        } else {
            [CVPSession setRestartMonitoringTimeInterval:@(value)];
        }
    } else if (textField == self.checkAccuracyTimerTextField) {
        [CVPSession setCheckAccuracyTimeInterval:@(value)];
    } else if (textField == self.monitoringBufferTimeTextField) {
        [CVPSession setMonitoringBufferTimeInterval:@(value * CVPMinuteTimeInterval)];
    }
    
    [textField resignFirstResponder];
    [self reload];
}

- (IBAction)cancelEditingAction:(id)sender {
    [[self.view findFirstResponder] resignFirstResponder];
    [self reload];
}
- (IBAction)autoPauseByiOSAction:(id)sender {
    [CVPSession setAutoPauseLocationByiOSEnabled:self.autoPauseByiOSSwtich.isOn];
    [[CVPLocationManager continousInstance] setAutoPauseByiOS:self.autoPauseByiOSSwtich.isOn];
}

- (IBAction)alertsAction:(id)sender {
    [CVPSession setShowAlerts:self.alertsSwitch.on];
}

- (IBAction)significantChangesAction:(id)sender {
    [CVPSession setSignificantChanges:self.significatChangesSwitch.on];
    if (self.significatChangesSwitch.on) {
        [[CVPositioning globalInstance] startUpdatingLocation];
//        [[HarvestManager instance] locationManagerForTrigger:LocationTriggerSignificantChanges];
//        [[SignificantLocationManager instance] startMonitoringSignificantLocationChanges];
    } else {
        [significantInstance stopMonitoringSignificantLocationChanges];
        significantInstance = nil;
//        [[SignificantLocationManager instance] stopMonitoringSignificantLocationChanges];
//        [[HarvestManager instance] releaseLocationManagerForTrigger:LocationTriggerSignificantChanges];
    }
}

- (IBAction)geofenceAction:(id)sender {
    [CVPSession setGeofenceEnabled:self.geofenceSwitch.on];
    if (self.geofenceSwitch.on) {
        [[CVPositioning globalInstance] startUpdatingLocation];
//        [[HarvestManager instance] locationManagerForTrigger:LocationTriggerGeofence];
//        [[GeofencigLocationManager instance] startRegionMonitoring];
    } else {
        [geofenceInstance stopRegionMonitoring];
        geofenceInstance = nil;
//        [[GeofencigLocationManager instance] stopRegionMonitoring];
//        [[HarvestManager instance] releaseLocationManagerForTrigger:LocationTriggerGeofence];
    }
    [self reload];
}

- (IBAction)visitsAction:(id)sender {
    [CVPSession setVisitsEnabled:self.visitsSwitch.on];
    if (self.visitsSwitch.on) {
        [[CVPositioning globalInstance] startUpdatingLocation];
        //        [[HarvestManager instance] locationManagerForTrigger:LocationTriggerGeofence];
        //        [[GeofencigLocationManager instance] startRegionMonitoring];
    } else {
        [visitInstance stopMonitoringVisits];
        visitInstance = nil;
        //        [[GeofencigLocationManager instance] stopRegionMonitoring];
        //        [[HarvestManager instance] releaseLocationManagerForTrigger:LocationTriggerGeofence];
    }
}

- (IBAction)continousMonitoringAction:(id)sender {
    [CVPSession setContinousMonitoringEnabled:self.continousMonitoringSwitch.on];
    if (self.continousMonitoringSwitch.on) {
        [[CVPLocationManager continousInstance] startUpdatingLocation:@"from button"];
    } else {
        [[CVPLocationManager continousInstance] stopUpdatingLocation];
        continousInstance = nil;
    }
}

- (IBAction)deferredUpdatesAction:(id)sender {
    [CVPSession setDeferEnabled:self.deferSwitch.on];
    if (! self.deferSwitch.on) {
        [[CVPLocationManager continousInstance] disallowDeferredLocationUpdates];
    }
    
    [self reload];
}

- (IBAction)decreaseAccuracyAction:(id)sender {
    [CVPSession setDecreaseAccuracyEnabled:self.decreaseAccuracySwitch.isOn];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.activityTypeTextField) {
        CLActivityType activityType = [CVPSession activityType].integerValue;
        NSInteger index = [self.activityTypes indexOfObject:@(activityType)];
        
        UIPickerView *pickerView = (UIPickerView *)textField.inputView;
        [pickerView selectRow:index inComponent:0 animated:NO];
    }
    @weakify(self);
    self.tapGestureRecognizer = [[TTTouchRecognizer alloc] initWithCallback:^{
        @strongify(self);
        [[self.view findFirstResponder] resignFirstResponder];
    } ignoreViews:@[self.activityTypeTextField, self.distanceTextField, self.geofenceRadiusTextField]];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.tapGestureRecognizer.view removeGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer = nil;
}

#pragma mark - Helpers

- (void)reload {
    self.alertsSwitch.on = [CVPSession showAlerts];
    self.significatChangesSwitch.on = [CVPSession significantChanges];
    self.geofenceSwitch.on = [CVPSession geofenceEnabled];
    self.deferSwitch.on = [CVPSession deferEnabled];
    self.autoPauseByiOSSwtich.on = [CVPSession autoPauseLocationByiOSEnabled];
    self.continousMonitoringSwitch.on = [CVPSession continousMonitoringEnabled];
    self.decreaseAccuracySwitch.on = [CVPSession decreaseAccuracyEnabled];
    self.visitsSwitch.on = [CVPSession visitsEnabled];
    
    self.activityTypeTextField.text = [self activityTypeString:[CVPSession activityType]];
    self.accuracyTextField.text = [self.accuracyDataSource nameForAccuracy:[CVPSession locationAccuracy].doubleValue];
    self.distanceTextField.text = NSStringFormat(@"%@", [CVPSession updateDistance]);
    self.geofenceRadiusTextField.text = NSStringFormat(@"%@", [CVPSession geofenceRadius]);
    self.deferDistanceTextField.text = NSStringFormat(@"%@", [CVPSession deferDistance]);
    self.deferIntervalTextField.text = NSStringFormat(@"%@", @([CVPSession deferTimeInterval].doubleValue / CVPMinuteTimeInterval));
    self.restartTimerTextField.text = NSStringFormat(@"%@", [CVPSession restartMonitoringTimeInterval]);
    self.checkAccuracyTimerTextField.text = NSStringFormat(@"%@", @([CVPSession checkAccuracyTimeInterval].doubleValue));
    self.monitoringBufferTimeTextField.text = NSStringFormat(@"%@", @([CVPSession monitoringBufferInterval].doubleValue / CVPMinuteTimeInterval));
    self.delayedBackgroundSyncSwtich.on = [CVPSession isDelayedBackgroundSync];
    self.motionProcessorSwitch.on = [CVPSession motionProcessorEnabled];
    
    BOOL enabled = [CVPSession geofenceEnabled];
    UIColor *color = enabled ? [UIColor blackColor] : [UIColor grayColor];
    [self.geofenceLabels setValue:color forKeyPath:@"textColor"];
    [self.geofenceTextFields setValue:color forKeyPath:@"textColor"];
    [self.geofenceTextFields setValue:@(enabled) forKeyPath:@"userInteractionEnabled"];
    
    enabled = [CVPSession deferEnabled];
    color = enabled ? [UIColor blackColor] : [UIColor grayColor];
    [self.deferLabels setValue:color forKeyPath:@"textColor"];
    [self.deferTextFields setValue:color forKeyPath:@"textColor"];
    [self.deferTextFields setValue:@(enabled) forKeyPath:@"userInteractionEnabled"];
    
    BOOL invalidDeferedAccuracy = ([CVPSession locationAccuracy].doubleValue != kCLLocationAccuracyBestForNavigation);
    if (invalidDeferedAccuracy) {
        invalidDeferedAccuracy = ([CVPSession locationAccuracy].doubleValue != kCLLocationAccuracyBest);
    }
    if (self.deferSwitch.isOn && invalidDeferedAccuracy) {
        self.deferSwitch.on = NO;
        [self deferredUpdatesAction:nil];
        
        [[[UIAlertView alloc] initWithTitle:nil message:@"Selected monitoring acurracy not suported by deferred updates" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

- (NSString *)activityTypeString:(NSNumber *)activityType {
    NSString *string = nil;
    switch (activityType.integerValue) {
        case CLActivityTypeOther:
            string = NSLocalizedString(@"Other", nil);
            break;
        case CLActivityTypeAutomotiveNavigation:
            string = NSLocalizedString(@"Automotive navigation", nil);
            break;
        case CLActivityTypeFitness:
            string = NSLocalizedString(@"Fitness", nil);
            break;
        case CLActivityTypeOtherNavigation:
            string = NSLocalizedString(@"Other navigation", nil);
            break;
            
        default:
            break;
    }
    return string;
}

- (IBAction)decreaseMonitoringAcurracy:(id)sender {
    [[CVPLocationManager continousInstance] decreaseMonitoringQualityWithTrigger:@"from button"];
}

- (IBAction)delayedBackgroundSyncAction:(id)sender {
    [CVPSession setIsDelayedBackgroundSync:self.delayedBackgroundSyncSwtich.isOn];
}

- (IBAction)motionProcessorAction:(id)sender {
    [CVPSession setMotionProcessorEnabled:self.motionProcessorSwitch.isOn];
    
    if (! [CVPSession motionProcessorEnabled]) {
        motionDetector = nil;
    }
}

@end
