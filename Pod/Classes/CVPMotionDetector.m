;//
//  MotionDetector.m
//  Positioning
//
//  Created by Alexandru Tudose on 16/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "CVPMotionDetector.h"
#import "CVPSession+Alerts.h"
//#import "Debug.h"
//#import "EXTScope.h"

@interface CVPMotionDetector ()
@property (strong, nonatomic, nonnull) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) CMMotionActivity *lastActivity;
@property (nonatomic, strong) NSDate *lastDate;
@end

@implementation CVPMotionDetector

CVPMotionDetector *motionDetector = nil;

+ (instancetype)instance {
    if (! motionDetector) {
        motionDetector = [[self alloc] init];
    }
    return motionDetector;
}

- (instancetype)init
{
    if (! [CMMotionActivityManager isActivityAvailable]) {
        return nil;
    }
    
    if (! [CVPSession motionProcessorEnabled]) {
        return nil;
    }
    
    self = [super init];
    if (self) {
        self.motionActivityManager = [[CMMotionActivityManager alloc] init];
        
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    return self;
}

- (void)dealloc {
    [self.motionActivityManager stopActivityUpdates];
    self.motionActivityManager = nil;
}

- (void)triggerAuthorizationCheck {
    NSLog(@"trigger authorization check");
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-60 * 1];
    [self.motionActivityManager queryActivityStartingFromDate:startDate toDate:[NSDate date] toQueue:[NSOperationQueue mainQueue] withHandler:^(NSArray<CMMotionActivity *> * _Nullable activities, NSError * _Nullable error) {
        NSLog(@"%@", activities);
    }];
}


- (void)fetchHistoryActivities:(void(^)(NSArray *activities, NSError *error))callback {
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-60 * 60 * 24 * 6];
    [self.motionActivityManager queryActivityStartingFromDate:startDate toDate:[NSDate date] toQueue:[NSOperationQueue mainQueue] withHandler:^(NSArray<CMMotionActivity *> * _Nullable activities, NSError * _Nullable error) {
        if (callback) {
            callback(activities, error);
        }
    }];
}

- (void)didMovedInLastHour:(void(^)(BOOL didMove))callback {
    NSTimeInterval checkInterval = [CVPSession monitoringBufferInterval].doubleValue;
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-checkInterval];
    
    [self.motionActivityManager queryActivityStartingFromDate:startDate toDate:[NSDate date] toQueue:[NSOperationQueue mainQueue] withHandler:^(NSArray<CMMotionActivity *> * _Nullable activities, NSError * _Nullable error) {
        NSLog(@"%@", activities);
        
        BOOL didFoundMovement = [self processActivitiesForMovement:activities];
        callback(didFoundMovement);
        
        self.lastActivity = [activities lastObject];
        self.lastDate = [NSDate date];
        
//        if (self.lastActivity) {
//            NSString *message = [NSString stringWithFormat:@"%@",  self.lastActivity];
//            [Session showAlertWithTitle:message];
//        }

    }];
}

- (BOOL)processActivitiesForMovement:(NSArray *)activities {
    CMMotionActivity *activity = [activities lastObject];
    if (! activity) {
        return NO;
    }

    NSInteger index = 0;
    for (CMMotionActivity *activity in activities.reverseObjectEnumerator.allObjects) {
        if (activity.automotive || activity.cycling || activity.running) {
            if (activity.confidence != CMMotionActivityConfidenceLow) {
                NSString *motion = activity.automotive ? @"automotive" : @"cycling";
                if (activity.running) {
                    motion = @"running";
                }
                NSString *message = [NSString stringWithFormat:@"Motion detected - %@ %@",  motion, [self.dateFormatter stringFromDate:activity.startDate]];
                [CVPSession showAlertWithTitle:message];
                return YES;
            }
        }
        
        if (activity.walking && activity.confidence == CMMotionActivityConfidenceHigh) {
            NSString *message = [NSString stringWithFormat:@"Motion detected - walking high : %@",  [self.dateFormatter stringFromDate:activity.startDate]];
            [CVPSession showAlertWithTitle:message];
            return YES;
        }
        
        index++;
    }
    
    return NO;
}

- (void)setMotionDetectedCallback:(void (^)(NSString *))motionDetectedCallback {
    _motionDetectedCallback = motionDetectedCallback;
    
    if (motionDetectedCallback) {
        @weakify(self)
        [self.motionActivityManager startActivityUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMMotionActivity * _Nullable activity) {
            @strongify(self)
            if ([self processActivitiesForMovement:@[activity]]) {
                motionDetectedCallback(@"Reason not implemented yet");
            }
        }];
    } else {
        [self.motionActivityManager stopActivityUpdates];
    }
}

@end
