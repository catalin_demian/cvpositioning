//
//  ActivityCellController.m
//  Positioning
//
//  Created by Alexandru Tudose on 16/10/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "ActivityCellController.h"
#import "ActivityCell.h"
#import <CoreMotion/CoreMotion.h>

@interface ActivityCellController ()
@property (strong, nonatomic, nonnull) NSDateFormatter *dateFormatter;
@end

@implementation ActivityCellController

- (id)init {
    if ( ( self = [super initWithCellNibName:@"ActivityCell"
                                  contentSize:CGSizeMake(-1, 44)]) ) {
        self.minimumInteritemSpacing = 0.0;
        self.minimumLineSpacing = 0.0;
        self.sectionInset = UIEdgeInsetsZero;
        
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    return self;
}

- (BOOL)acceptsContent:(id)content {
    return [content isKindOfClass:[CMMotionActivity class]];
}

- (void)configureCell:(ActivityCell *)cell forContent:(CMMotionActivity *)content indexPath:(NSIndexPath *)indexPath {
    NSString *message = @" ";
    if (content.walking) {
        message = [message stringByAppendingString:@"walking "];
    } if (content.running) {
        message = [message stringByAppendingString:@"running "];
    } if (content.automotive) {
        message = [message stringByAppendingString:@"automotive "];
    } if (content.cycling) {
        message = [message stringByAppendingString:@"cycling "];
    } if (content.stationary) {
        message = [message stringByAppendingString:@"stationary "];
    } if (content.unknown) {
        message = [message stringByAppendingString:@"unknown"];
    }
    
    if ([message isEqualToString:@" "]) {
        message = @" device could be in motion";
    }
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@ - %ld", message, content.confidence];
    cell.subtitleLabel.text = [self.dateFormatter stringFromDate:content.startDate];

    cell.rightView.hidden = YES;
//    // entry color
//    cell.rightView.hidden = content.confidence == CMMotionActivityConfidenceLow;
//    if (! cell.rightView.hidden) {
//        if (content.confidence == CMMotionActivityConfidenceMedium) {
//            cell.rightView.backgroundColor = [UIColor orangeColor];
//        } else if (content.confidence == CMMotionActivityConfidenceHigh) {
//            cell.rightView.backgroundColor = [UIColor purpleColor];
//        }
//    }
}

@end
