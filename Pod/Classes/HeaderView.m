//
//  ZOHeaderView.m
//  Zonga
//
//  Created by Andrei Dumitru on 4/13/14.
//  Copyright (c) 2014 Trilulilu. All rights reserved.
//

#import "HeaderView.h"
//#import "Debug.h"

@implementation HeaderView

- (id)init {
    return NSMainBundleLoadNib(NSStringFromClass([HeaderView class]), nil);
}

@end
