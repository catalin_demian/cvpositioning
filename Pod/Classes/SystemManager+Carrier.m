//
//  SystemManager+Carrier.m
//  Convoyz
//
//  Created by Andrei Dumitru on 7/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager+Carrier.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@implementation SystemManager (Carrier)

- (NSString *)mobileCountryCode {
    return  [self.carrier mobileCountryCode];
}

- (NSString *)mobileNetworkCode {
    return  [self.carrier mobileNetworkCode];
}

- (NSString *)isoCountryCode {
    return  [self.carrier isoCountryCode];
}

#pragma mark - Helpers

- (CTCarrier *)carrier {
    CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
    return [netInfo subscriberCellularProvider];
}

@end
