//
//  SystemInfoSerializer.h
//  Convoyz
//
//  Created by Andrei Dumitru on 6/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemInfoSerializer : NSObject

+ (NSDictionary *)currentSystemInfoSerialized;

@end
