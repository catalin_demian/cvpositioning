//
//  CMMotionActivity+Custom.m
//  Convoyz
//
//  Created by Andrei Dumitru on 7/27/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CMMotionActivity+Custom.h"

@implementation CMMotionActivity (Custom)

- (NSString *)confidenceString {
    NSString *string = nil;
    switch (self.confidence) {
        case CMMotionActivityConfidenceLow:
            string = @"MotionActivityConfidenceLow";
            break;
        case CMMotionActivityConfidenceMedium:
            string = @"MotionActivityConfidenceMedium";
            break;
        case CMMotionActivityConfidenceHigh:
            string = @"MotionActivityConfidenceHigh";
            break;
            
        default:
            break;
    }
    return string;
}

@end
