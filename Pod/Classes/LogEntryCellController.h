//
//  LogEntryCellController.h
//  Positioning
//
//  Created by Alexandru Tudose on 12/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Tapptitude/TTBlockCollectionCellController.h>

@interface LogEntryCellController : TTBlockCollectionCellController

@end
