//
//  System+Network.m
//  Convoyz
//
//  Created by Andrei Dumitru on 5/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager+Network.h"
#import <CoreFoundation/CoreFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>

#import <SystemConfiguration/CaptiveNetwork.h>

@implementation SystemManager (Network)

- (NetworkStatus)networkStatus {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachabilityRef = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr *) &zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    NetworkStatus status = NotReachable;
    if (!SCNetworkReachabilityGetFlags(reachabilityRef, &flags)) {
        return status;
    }
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0) {
        status = ReachableViaWiFi;
    }
    
    if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand) != 0) || (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0)) {
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0) {
            status = ReachableViaWiFi;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN) {
        status = ReachableViaWWAN;
    }
    CFRelease(reachabilityRef);
    return status;
}

- (NSString *)networkStatusString {
    NSString *string = nil;
    switch (self.networkStatus) {
        case NotReachable:
            string = @"NotReachable";
            break;
        case ReachableViaWiFi:
            string = @"ReachableViaWiFi";
            break;
        case ReachableViaWWAN:
            string = @"ReachableViaWWAN";
            break;
        default:
            break;
    }
    return string;
}

- (NSDictionary *)BSSIDDictionary {
    if (self.networkStatus != ReachableViaWiFi) {
        return nil;
    }
    NSMutableArray *infos = [[NSMutableArray alloc] init];
    NSArray *interfaces = (__bridge_transfer NSArray *)CNCopySupportedInterfaces();
    
    for (NSString *interface in interfaces) {
        NSDictionary *info = (__bridge_transfer NSDictionary *)CNCopyCurrentNetworkInfo((__bridge CFStringRef)interface);
        if (info) {
            [infos addObject:info];
        }
    }
//    Example info
//    {
//        BSSID = "e0:3f:49:0:3e:4";
//        SSID = "tapp_etj2";
//        SSIDDATA = <74617070 5f65746a 32>;
//    }
    return [infos lastObject];
}

- (NSString *)BSSIDString {
    return self.BSSIDDictionary[@"BSSID"];
}

@end
