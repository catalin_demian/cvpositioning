//
//  SystemManager+Version.m
//  Convoyz
//
//  Created by Andrei Dumitru on 7/24/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager+Version.h"

@implementation SystemManager (Version)

- (NSString *)appVersion {
    NSDictionary *dictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [dictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [dictionary objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"%@(%@)", version, build];
}

@end
