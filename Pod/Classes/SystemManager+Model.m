//
//  System+Model.m
//  Convoyz
//
//  Created by Andrei Dumitru on 5/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager+Model.h"
#import <sys/utsname.h>

@implementation SystemManager (Model)

- (NSString *)deviceModel {
    NSString *string = nil;
    if ([[UIDevice currentDevice] respondsToSelector:@selector(model)]) {
        string = [[UIDevice currentDevice] model];
    }
    return string;
}

- (NSString *)deviceName {
    NSString *string = nil;
    if ([[UIDevice currentDevice] respondsToSelector:@selector(name)]) {
        string = [[UIDevice currentDevice] name];
    }
    return string;
}

- (NSString *)deviceType {
    struct utsname deviceType;
    uname(&deviceType);
    return [NSString stringWithFormat:@"%s", deviceType.machine];
}

- (NSString *)systemName {
    NSString *string = nil;
    if ([[UIDevice currentDevice] respondsToSelector:@selector(systemName)]) {
        string = [[UIDevice currentDevice] systemName];
    }
    return string;
}

- (NSString *)systemVersion {
    NSString *string = nil;
    if ([[UIDevice currentDevice] respondsToSelector:@selector(systemVersion)]) {
        string = [[UIDevice currentDevice] systemVersion];
    }
    return string;
}

@end
