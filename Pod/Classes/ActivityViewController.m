//
//  VSFViewController.m
//  Visor
//
//  Created by Alexandru Tudose on 02.12.2014.
//  Copyright (c) 2014 Visor. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActivityCellController.h"
#import "CVPMotionDetector.h"
#import "HeaderView.h"
#import "CMMotionActivity+Section.h"
#import <Tapptitude/TTSectionedDataSource.h>
#import <CVPSession.h>

@interface ActivityViewController () <UIAlertViewDelegate>

@end

@implementation ActivityViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.cellController = [[ActivityCellController alloc] init];
        self.title = @"Motion History";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerCollectionHeaderView];
    UICollectionViewFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    layout.sectionHeadersPinToVisibleBounds = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (![CVPSession motionProcessorEnabled]) {
        [[[UIAlertView alloc] initWithTitle:@"Info" message:@"Motion tracking has not been activated yet. Do you want to activate it now?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil] show];
        return;
    }
    
    [self loadData];
}

- (void)loadData {
    [self.reloadIndicatorView startAnimating];
    [[CVPMotionDetector instance] fetchHistoryActivities:^(NSArray *activities, NSError *error) {
        [self.reloadIndicatorView stopAnimating];
        
        if (error) {
            //            [self checkAndShowError:error];
        } else {
            NSArray *items = activities.reverseObjectEnumerator.allObjects;
            items = [self sectionedContentForContent:items keyPath:@"sectionTitle"];
            self.dataSource = [[TTSectionedDataSource alloc] initWithStaticContent:items];
        }
    }];
}

- (void)registerCollectionHeaderView {
    NSString *nibName = NSStringFromClass([HeaderView class]);
    UINib *headerNib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:nibName];
    
    UIView *headerView = [[headerNib instantiateWithOwner:nil options:nil] lastObject];
    UICollectionViewFlowLayout *flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.headerReferenceSize = headerView.bounds.size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        CMMotionActivity *entry = [self.dataSource objectAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:indexPath.section]];
        
        NSString *nibName = NSStringFromClass([HeaderView class]);
        HeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:nibName forIndexPath:indexPath];
        headerView.headerLabel.text = entry.sectionTitle;
        return headerView;
    } else {
        return [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
}

- (NSArray *)sectionedContentForContent:(NSArray *)content keyPath:(NSString *)keyPath {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableArray *sectionedContent = [NSMutableArray array];
    
    for (id item in content) {
        NSString *title = [item valueForKeyPath:keyPath];
        NSMutableArray *section = [dict valueForKeyPath:title];
        if (! section) {
            section = [NSMutableArray array];
            
            [sectionedContent addObject:section];
            [dict setValue:section forKey:title];
        }
        
        [section addObject:item];
    }
    
    return sectionedContent;
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [CVPSession setMotionProcessorEnabled:YES];
        [self loadData];
    }
}

@end
