//
//  LocationSerializer.m
//  Convoyz
//
//  Created by Andrei Dumitru on 6/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPLocationSerializer.h"
#import "NSDate+Timestamp.h"

@implementation CVPLocationSerializer

+ (NSArray *)serializeLocations:(NSArray *)content {
    NSMutableArray *locations = [NSMutableArray array];
    for (CLLocation *location in content) {
        NSDictionary *dictionary = [self serializeLocation:location];
        [locations addObject:dictionary];
    }
    return locations.copy;
}

+ (NSDictionary *)serializeLocation:(CLLocation *)location {
    if (!location) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(location.coordinate.latitude) forKey:@"latitude"];
    [dictionary setValue:@(location.coordinate.longitude) forKey:@"longitude"];
    [dictionary setValue:@(location.altitude) forKey:@"altitude"];
    [dictionary setValue:@(location.horizontalAccuracy) forKey:@"horizontalAccuracy"];
    [dictionary setValue:@(location.verticalAccuracy) forKey:@"verticalAccuracy"];
    [dictionary setValue:@(location.course) forKey:@"course"];
    [dictionary setValue:@(location.speed) forKey:@"speed"];
    [dictionary setValue:@(location.timestamp.timestamp) forKey:@"timestamp"];
    [dictionary setValue:@(location.floor.level) forKey:@"floorLevel"];
    
    return dictionary;
}

+ (NSDictionary *)serializeHeading:(CLHeading *)heading {
    if (!heading) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(heading.magneticHeading) forKey:@"magneticHeading"];
    [dictionary setValue:@(heading.trueHeading) forKey:@"trueHeading"];
    [dictionary setValue:@(heading.headingAccuracy) forKey:@"headingAccuracy"];
    [dictionary setValue:@(heading.x) forKey:@"headingComponentValueX"];
    [dictionary setValue:@(heading.y) forKey:@"headingComponentValueY"];
    [dictionary setValue:@(heading.z) forKey:@"headingComponentValueZ"];
    [dictionary setValue:@(heading.timestamp.timestamp) forKey:@"timestamp"];
    
    return dictionary;
}

@end
