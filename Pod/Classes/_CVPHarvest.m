// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CVPHarvest.m instead.

#import "_CVPHarvest.h"

const struct CVPHarvestAttributes CVPHarvestAttributes = {
	.content = @"content",
	.date = @"date",
	.harvestID = @"harvestID",
	.priority = @"priority",
	.source = @"source",
	.status = @"status",
	.timezone = @"timezone",
	.type = @"type",
};

@implementation CVPHarvestID
@end

@implementation _CVPHarvest

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Harvest" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Harvest";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Harvest" inManagedObjectContext:moc_];
}

- (CVPHarvestID*)objectID {
	return (CVPHarvestID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"priorityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"priority"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sourceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"source"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"statusValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"status"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"typeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"type"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic content;

@dynamic date;

@dynamic harvestID;

@dynamic priority;

- (int32_t)priorityValue {
	NSNumber *result = [self priority];
	return [result intValue];
}

- (void)setPriorityValue:(int32_t)value_ {
	[self setPriority:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePriorityValue {
	NSNumber *result = [self primitivePriority];
	return [result intValue];
}

- (void)setPrimitivePriorityValue:(int32_t)value_ {
	[self setPrimitivePriority:[NSNumber numberWithInt:value_]];
}

@dynamic source;

- (int32_t)sourceValue {
	NSNumber *result = [self source];
	return [result intValue];
}

- (void)setSourceValue:(int32_t)value_ {
	[self setSource:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSourceValue {
	NSNumber *result = [self primitiveSource];
	return [result intValue];
}

- (void)setPrimitiveSourceValue:(int32_t)value_ {
	[self setPrimitiveSource:[NSNumber numberWithInt:value_]];
}

@dynamic status;

- (int32_t)statusValue {
	NSNumber *result = [self status];
	return [result intValue];
}

- (void)setStatusValue:(int32_t)value_ {
	[self setStatus:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveStatusValue {
	NSNumber *result = [self primitiveStatus];
	return [result intValue];
}

- (void)setPrimitiveStatusValue:(int32_t)value_ {
	[self setPrimitiveStatus:[NSNumber numberWithInt:value_]];
}

@dynamic timezone;

@dynamic type;

- (int32_t)typeValue {
	NSNumber *result = [self type];
	return [result intValue];
}

- (void)setTypeValue:(int32_t)value_ {
	[self setType:[NSNumber numberWithInt:value_]];
}

@end

