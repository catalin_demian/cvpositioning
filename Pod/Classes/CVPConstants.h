//
//  CVPConstants.h
//  CVPositioning
//
//  Created by Catalin Demian on 2/2/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


extern const struct CVPSessionSpace {
    __unsafe_unretained NSString *applicationKillDate;
    __unsafe_unretained NSString *showAlerts;
    __unsafe_unretained NSString *activityType;
    __unsafe_unretained NSString *accuracy;
    __unsafe_unretained NSString *updateDistance;
    __unsafe_unretained NSString *significantChanges;
    __unsafe_unretained NSString *geofenceEnabled;
    __unsafe_unretained NSString *geofenceRadius;
    __unsafe_unretained NSString *deferEnabled;
    __unsafe_unretained NSString *deferDistance;
    __unsafe_unretained NSString *deferTimeInterval;
    __unsafe_unretained NSString *restartMonitoringTimeInterval;
    __unsafe_unretained NSString *autoPauseLocationByiOS;
    __unsafe_unretained NSString *continousMonitoring;
    __unsafe_unretained NSString *decreaseAccuracyEnabled;
    __unsafe_unretained NSString *checkAccuracyTimeInterval;
    __unsafe_unretained NSString *monitoringBufferTimeInterval;
    __unsafe_unretained NSString *isDelayedBackgroundSync;
    __unsafe_unretained NSString *motionProcessorEnabled;
    __unsafe_unretained NSString *visitsEnabled;
} CVPSessionSpace;

typedef NS_ENUM(NSInteger, CVPLocationTrigger) {
    CVPLocationTriggerUnknown = 0,
    CVPLocationTriggerContinousMonitoring,
    CVPLocationTriggerGeofence,
    CVPLocationTriggerSignificantChanges,
    CVPLocationTriggerVisitExtension,
    CVPLocationTriggerContinousMonitoring_LowAccuracy,
    CVPLocationTriggerContinousMonitoring_WiFiChanged,
    CVPLocationTriggerContinousMonitoring_MotionChanged,
};

extern const struct CVPAlertSource {
    __unsafe_unretained NSString *visit;
} CVPAlertSource;

static NSTimeInterval const CVPMinuteTimeInterval = 60;
static NSTimeInterval const CVPHourTimeInterval = 60 * CVPMinuteTimeInterval;
static NSTimeInterval const CVPDayTimeInterval = 24 * CVPHourTimeInterval;
static NSTimeInterval const CVPHarvestUserStayInterval = 7 * CVPDayTimeInterval;
static NSTimeInterval const CVPDefaultGeofenceTimeInterval = CVPMinuteTimeInterval;
static NSTimeInterval const CVPDefaultDeferTimeInterval = 5 * CVPMinuteTimeInterval;
static NSTimeInterval const CVPRestartMonitoringAfterTimeInterval = 0;
static NSTimeInterval const CVPBackgroundMonitoringTimeInterval = 10;
static NSTimeInterval const CVPMonitoringBufferTimeInterval = 15 * CVPMinuteTimeInterval;
static NSTimeInterval const CVPCheckAccuracyTimeInterval = 1 * CVPMinuteTimeInterval;


static NSString * const CVPDidExitGeofenceRegionNotification = @"DidExitGeofenceRegionNotification";
static NSString * const CVPLocationDidUpdateNotification = @"LocationDidUpdateNotification";

static  CLLocationDistance const CVPDefaultUpdateDistance = 15.0;
static  CLLocationDistance const CVPDefaultGeofenceRadius = 25.0;
static  CLLocationDistance const CVPDefaultDeferDistancte = 100.0;
