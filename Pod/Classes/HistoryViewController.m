//
//  HistoryViewController.m
//  Positioning
//
//  Created by Andrei Dumitru on 10/7/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryCellController.h"
#import "LogEntryCellController.h"
#import "CVPHarvest.h"
#import "CVPConstants.h"
#import "CVPLogEntry.h"
#import "HeaderView.h"
#import <Tapptitude/TTFetchedDataSource.h>
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalRequests.h>
#import "CVPVisitHistoryViewController.h"

@interface HistoryViewController ()
@property (nonatomic, strong) NSDateFormatter *headerDateFormatter;
@end

@implementation HistoryViewController

- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.showOnlyVisits?NSLocalizedString(@"Visits", nil):NSLocalizedString(@"History", nil);
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStyleDone target:self action:@selector(showMap)];
    self.navigationItem.rightBarButtonItem = item;
    
    [self registerCollectionHeaderView];
    UICollectionViewFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    layout.sectionHeadersPinToVisibleBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationDidUpdateNotification:) name:CVPLocationDidUpdateNotification object:nil];
    NSArray *controllers = @[[[HistoryCellController alloc] init], [[LogEntryCellController alloc ] init]];
    TTMultiCollectionCellController *cellController = [[TTMultiCollectionCellController alloc] initWithControllers:controllers];
    self.cellController = cellController;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Load/Reload data source when the screen will appear
    [self locationDidUpdateNotification:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //Load/Reload data source when the screen will appear
    [self locationDidUpdateNotification:nil];
}

#pragma mark - Notifications

- (void)locationDidUpdateNotification:(NSNotification *)notification {
    if (! self.view.window) {
        return; // ignore while hidden
    }
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type == %@", @(HarvestLocationData)];
    
//    NSArray *locations = [Harvest MR_findAllWithPredicate:predicate];
//    [content addObjectsFromArray:locations];
//    NSTimeInterval days = 2 * 24 * 60 * 60;
//    predicate = [NSPredicate predicateWithFormat:@"date > %@", [NSDate dateWithTimeIntervalSinceNow:days]];
//    NSArray *allLogs = [LogEntry MR_findAll];
//    [content addObjectsFromArray:allLogs];
//    [content sortUsingDescriptors:@[sortDescriptor]];
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"source == %@", self.showOnlyVisits?@"Visit":nil];
    NSFetchRequest *request = [CVPLogEntry MR_requestAllSortedBy:@"date" ascending:NO withPredicate:predicate inContext:context];
    self.dataSource = [[TTFetchedDataSource alloc] initWithFetchRequest:request sectionKeyPath:@"sectionTitle" context:context];
}

- (void)registerCollectionHeaderView {
    NSString *nibName = NSStringFromClass([HeaderView class]);
    UINib *headerNib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:nibName];
    
    UIView *headerView = [[headerNib instantiateWithOwner:nil options:nil] lastObject];
    UICollectionViewFlowLayout *flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.headerReferenceSize = headerView.bounds.size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        CVPLogEntry *entry = [self.dataSource objectAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:indexPath.section]];
        
        NSString *nibName = NSStringFromClass([HeaderView class]);
        HeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:nibName forIndexPath:indexPath];
        headerView.headerLabel.text = entry.sectionTitle;
        return headerView;
    } else {
        return [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    }
}

#pragma mark - Actions 

- (void)showMap {
    [self.navigationController pushViewController:[[CVPVisitHistoryViewController alloc] init] animated:YES];
}

@end
