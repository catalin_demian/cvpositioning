//
//  CMMotionActivity+Section.h
//  Positioning
//
//  Created by Alexandru Tudose on 19/01/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>

@interface CMMotionActivity (Section)
- (NSString *)sectionTitle;
@end
