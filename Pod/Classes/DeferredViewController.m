//
//  DeferredViewController.m
//  Positioning
//
//  Created by Alexandru Tudose on 19/01/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "DeferredViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "NSError+CoreLocation.h"
#import "CVPSession.h"

@interface DeferredViewController ()<UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic, strong) NSMutableArray *locations;

@property (nonatomic, strong) NSMutableArray *singleLocationUpdates;

@property (nonatomic, strong) NSMutableArray *multiLocationUpdates;

@property (nonatomic, assign) NSInteger singleLocationsCountAfterDeferred;
@property (nonatomic, strong) NSMutableArray *deferredSingeLocationUpdates;
@end

@implementation DeferredViewController {
    //
    // flag to determine if we should try to allow deferred location updates
    BOOL _atLeastFiveLocations;
    
    //
    // if we are currently deferring, we  don't want to attempt to call allowDefer... again
    BOOL _deferring;
    
    //
    // date formatter to specify time of location updates
    NSDateFormatter *_medStyleDF;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.title = @"Deferred Updates Test";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    // create our date formatter
    _medStyleDF = [[NSDateFormatter alloc] init];
    [_medStyleDF setTimeStyle:NSDateFormatterMediumStyle];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
//    self.locationManager.activityType = CLActivityTypeFitness;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.allowsBackgroundLocationUpdates = YES;
    self.locationManager.pausesLocationUpdatesAutomatically = NO;
    
    self.locations = [@[]mutableCopy];
    self.multiLocationUpdates = [@[]mutableCopy];
    self.singleLocationUpdates = [@[]mutableCopy];
    self.deferredSingeLocationUpdates = [@[]mutableCopy];
    
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
}

#pragma mark UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"All locations";
    }
    else if (section == 1) {
        return @"Single location updates";
    }
    else {
        return @"Deferred location updates";
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return self.multiLocationUpdates.count;
    }
    else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    //
    // total location updates
    if (indexPath.section == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"%lu location updates", (unsigned long)self.locations.count];
        cell.detailTextLabel.text = nil;
    }
    //
    // regular location updates
    else if (indexPath.section == 1) {
        cell.textLabel.text = [NSString stringWithFormat:@"%lu single location updates", (unsigned long)self.singleLocationUpdates.count];
        cell.detailTextLabel.text = nil;
    }
    //
    // deferred location updates
    else {
        id item = self.multiLocationUpdates[indexPath.row];
        if ([item isKindOfClass:[NSString class]]) {
            cell.textLabel.text = item;
        } else {
            NSArray *updates = item;
            CLLocation *firstLoc = [updates firstObject];
            CLLocation *lastLoc = [updates lastObject];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%lu locations update @ %@", (unsigned long)updates.count, [NSDate date]];
            CLLocationDistance distance = [firstLoc distanceFromLocation:lastLoc];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2fm", distance];
        }
    }
    
    return cell;
}

#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error {
    NSLog(@"%s -- error: %@", __PRETTY_FUNCTION__, error);
    if (_singleLocationsCountAfterDeferred > 0) {
        NSString *message = [NSString stringWithFormat:@"Single locations update - Count: %ld", _singleLocationsCountAfterDeferred];
        [self.multiLocationUpdates insertObject:message atIndex:0];
        
        NSTimeInterval timeInterval = 0;
        NSDate *lastDate = nil;
        for (NSDate *date in self.deferredSingeLocationUpdates) {
            if (lastDate == nil) {
            } else {
                timeInterval += fabs([date timeIntervalSinceDate:lastDate]);
            }
            lastDate = date;
        }
        
        message = [NSString stringWithFormat:@"Duration between single updates: %.1fs", timeInterval];
        [self.multiLocationUpdates insertObject:message atIndex:0];
    }
    
    if (error != nil) {
        NSLog(@"Reason: %@", error.failedReason);
        NSString *message = [NSString stringWithFormat:@"%@ @ %@", error.failedReason, [_medStyleDF stringFromDate:[NSDate date]]];
        [self.multiLocationUpdates insertObject:message atIndex:0];
    } else {
        NSString *message = [NSString stringWithFormat:@"Deferred completed @ %@", [_medStyleDF stringFromDate:[NSDate date]]];
        [self.multiLocationUpdates insertObject:message atIndex:0];
    }
    
    _deferring = NO;
    self.singleLocationsCountAfterDeferred = 0;
    //    _atLeastFiveLocations = NO;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"%s -- %lu locations", __PRETTY_FUNCTION__, (unsigned long)locations.count);
    
    [self.locations addObjectsFromArray:locations];
    
    if (locations.count == 1) {
        [self.singleLocationUpdates addObject:locations];
        if (_deferring) {
            _singleLocationsCountAfterDeferred += 1;
            [self.deferredSingeLocationUpdates addObject:[NSDate date]];
        }
    }
    else if (locations.count > 1) {
        [self.multiLocationUpdates insertObject:locations atIndex:0];
    }
    
    //_atLeastFiveLocations = (self.locations.count >= 5);
    _atLeastFiveLocations = YES;
    
    BOOL isInBackground = [UIApplication sharedApplication].applicationState == UIApplicationStateBackground;
    if (!_deferring && _atLeastFiveLocations && [CLLocationManager deferredLocationUpdatesAvailable] && isInBackground) {
        NSString *message = [NSString stringWithFormat:@"Activated deffered updates @ %@", [_medStyleDF stringFromDate:[NSDate date]]];
        [self.multiLocationUpdates insertObject:message atIndex:0];
        NSLog(@"allow deferred location updates");
//        [self.locationManager allowDeferredLocationUpdatesUntilTraveled:CLLocationDistanceMax timeout:CLTimeIntervalMax];
        double distance = [CVPSession deferDistance].doubleValue;
        double timeinterval = [CVPSession deferTimeInterval].doubleValue;
        [self.locationManager allowDeferredLocationUpdatesUntilTraveled:distance timeout:timeinterval];
        _deferring = YES;
        self.singleLocationsCountAfterDeferred = 0;
        [self.deferredSingeLocationUpdates removeAllObjects];
    }
    
    [self.tableView reloadData];
}

@end