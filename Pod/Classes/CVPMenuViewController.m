//
//  CVPMenuViewController.m
//  CVPositioning
//
//  Created by Catalin Demian on 2/4/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPMenuViewController.h"
#import "CVPositioning+Screens.h"

@interface CVPMenuViewController ()

@end

@implementation CVPMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (IBAction)motionAction:(id)sender {
    [self.navigationController pushViewController:[CVPositioning motionViewController] animated:YES];
}

- (IBAction)historyAction:(id)sender {
    [self.navigationController pushViewController:[CVPositioning historyViewController] animated:YES];
}

- (IBAction)settingsAction:(id)sender {
    [self.navigationController pushViewController:[CVPositioning settingsViewController] animated:YES];
}

- (IBAction)visitsAction:(id)sender {
    [self.navigationController pushViewController:[CVPositioning visitsViewController] animated:YES];
}

@end
