#import "_CVPVisit.h"
#import <MapKit/MapKit.h>

@interface CVPVisit : _CVPVisit <MKAnnotation> {}

- (NSString *)sectionTitle;
- (NSString *)visitDescription;
- (NSString *)dateTimeString;

@end
