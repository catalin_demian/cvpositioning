//
//  CVPositioning+Screens.m
//  CVPositioning
//
//  Created by Catalin Demian on 2/4/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPositioning+Screens.h"
#import "SettingsViewController.h"
#import "HistoryViewController.h"
#import "CVPMenuViewController.h"
#import "ActivityViewController.h"
#import "CVPVisitHistoryViewController.h"

@implementation CVPositioning (Screens)

+ (UIViewController *)settingsViewController {
    return [[SettingsViewController alloc] init];
}

+ (UIViewController *)historyViewController {
    return [[HistoryViewController alloc] init];
}

+ (UIViewController *)motionViewController {
    return [[ActivityViewController alloc] init];
}

+ (UIViewController *)menuViewController {
    return [[UINavigationController alloc] initWithRootViewController:[[CVPMenuViewController alloc] init]];
}

+ (UIViewController *)visitsViewController {
    CVPVisitHistoryViewController *vc = [[CVPVisitHistoryViewController alloc] init];
    return vc;
}

@end
