//
//  NSError+CoreLocation.m
//  Positioning
//
//  Created by Alexandru Tudose on 19/01/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "NSError+CoreLocation.h"
#import <CoreLocation/CoreLocation.h>

@implementation NSError (CoreLocation)

- (NSString *)failedReason {
    if ([self.domain isEqualToString:kCLErrorDomain]) {
        NSString *message = @"unkown";
        switch (self.code) {
            case kCLErrorDeferredFailed:
                message = @"Deferred mode failed";
            case kCLErrorDeferredNotUpdatingLocation:
                message = @"Deferred mode failed because location updates disabled or paused";
            case kCLErrorDeferredAccuracyTooLow:
                message = @"Deferred mode not supported for the requested accuracy";
            case kCLErrorDeferredDistanceFiltered:
                message = @"Deferred mode does not support distance filters";
            case kCLErrorDeferredCanceled:
                message = @"Deferred Canceled";
            default:
                break;
        }
        
        return message;
    }
    
    return @"";
}

@end
