//
//  AccuracyPickerDataSource.m
//  Positioning
//
//  Created by Alexandru Tudose on 02/11/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "AccuracyPickerDataSource.h"


@interface AccuracyPickerDataSource ()
@property (nonatomic, strong) NSArray *activityAccuracy;
@property (nonatomic, strong) NSDictionary *activityValues;
@end

@implementation AccuracyPickerDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.activityAccuracy = @[@"BestForNavigation",
                                  @"Best",
                                  @"NearestTenMeters",
                                  @"HundredMeters",];
//                                  @"Kilometer",
//                                  @"ThreeKilometers"];
        
        self.activityValues = @{@"BestForNavigation" : @(kCLLocationAccuracyBestForNavigation),
                                @"Best" : @(kCLLocationAccuracyBest),
                                @"NearestTenMeters" : @(kCLLocationAccuracyNearestTenMeters),
                                @"HundredMeters" : @(kCLLocationAccuracyHundredMeters),
                                @"Kilometer" : @(kCLLocationAccuracyKilometer),
                                @"ThreeKilometers" : @(kCLLocationAccuracyThreeKilometers),};
    }
    return self;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger count = 0;
    if (component == 0) {
        count = self.activityAccuracy.count;
    }
    return count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component != 0) {
        return nil;
    }
    return self.activityAccuracy[row];
}


- (CLLocationAccuracy)accuracyForName:(NSString *)accuracyName {
    return [[self.activityValues valueForKey:accuracyName] doubleValue];
}

- (NSString *)nameForAccuracy:(CLLocationAccuracy)accuracy {
    __block NSString *foundKey = @"Not valid";
    
    [self.activityValues enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSNumber *obj, BOOL *stop) {
        if ([obj doubleValue] == accuracy) {
            *stop = YES;
            foundKey = key;
        }
    }];
    
    return foundKey;
}

- (NSString *)nameForIndex:(NSInteger)index {
    return self.activityAccuracy[index];
}

@end
