//
//  SystemManager+Version.h
//  Convoyz
//
//  Created by Andrei Dumitru on 7/24/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

@interface SystemManager (Version)

@property (readonly, nonatomic) NSString *appVersion;

@end
