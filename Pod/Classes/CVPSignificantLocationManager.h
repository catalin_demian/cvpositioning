//
//  SignificantLocationManager.h
//  Positioning
//
//  Created by Alexandru Tudose on 15/10/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CVPLocationManager.h"

@interface CVPSignificantLocationManager : NSObject

@property (readwrite, nonatomic) CVPLocationTrigger triggerMode;

@property (copy, nonatomic) void (^locationUpdateCallback)(NSArray *locations, CVPLocationTrigger trigger);
@property (copy, nonatomic) void (^errorCallback)(NSError *error);

+ (instancetype)instance;

- (void)startMonitoringSignificantLocationChanges;
- (void)stopMonitoringSignificantLocationChanges;

extern CVPSignificantLocationManager *significantInstance;


@end
