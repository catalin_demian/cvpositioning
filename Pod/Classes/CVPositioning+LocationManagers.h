//
//  CVPositioning+LocationManagers.h
//  CVPositioning
//
//  Created by Catalin Demian on 2/4/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPositioning.h"
#import "CVPLocationManager.h"

@interface CVPositioning (LocationManagers)

- (CVPLocationManager *)locationManagerForTrigger:(CVPLocationTrigger)trigger;
- (void)releaseLocationManagerForTrigger:(CVPLocationTrigger)trigger;


@end
