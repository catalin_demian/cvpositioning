//
//  ActivityCellController.h
//  Positioning
//
//  Created by Alexandru Tudose on 16/10/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Tapptitude/TTBlockCollectionCellController.h>

@interface ActivityCellController : TTBlockCollectionCellController

@end
