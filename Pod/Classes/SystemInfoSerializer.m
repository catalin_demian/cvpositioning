//
//  SystemInfoSerializer.m
//  Convoyz
//
//  Created by Andrei Dumitru on 6/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemInfoSerializer.h"
#import "SystemManager+CPU.h"
#import "SystemManager+Disk.h"
#import "SystemManager+Model.h"
#import "SystemManager+Network.h"
#import "SystemManager+Carrier.h"
#import "SystemManager+Version.h"

@implementation SystemInfoSerializer

+ (NSDictionary *)currentSystemInfoSerialized {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    SystemManager *instance = [SystemManager instance];
    [dictionary setValue:@(instance.batteryLevel) forKey:@"batteryLevel"];
    [dictionary setValue:@(instance.screenBrightness) forKey:@"screenBrightness"];
    [dictionary setValue:instance.applicationStateString forKey:@"applicationState"];
    [dictionary setValue:instance.cpuUsageString forKey:@"cpuUsage"];
    [dictionary setValue:instance.diskSpace forKey:@"diskSpace"];
    [dictionary setValue:instance.freeDiskSpace forKey:@"freeDiskSpace"];
    [dictionary setValue:instance.usedDiskSpace forKey:@"usedDiskSpace"];
    [dictionary setValue:instance.deviceModel forKey:@"deviceModel"];
    [dictionary setValue:instance.deviceName forKey:@"deviceName"];
    [dictionary setValue:instance.deviceType forKey:@"deviceType"];
    [dictionary setValue:instance.systemName forKey:@"systemName"];
    [dictionary setValue:instance.systemVersion forKey:@"systemVersion"];
    [dictionary setValue:instance.networkStatusString forKey:@"networkStatus"];
    [dictionary setValue:instance.BSSIDString forKey:@"networBSSID"];
    [dictionary setValue:instance.appVersion forKey:@"version"];
    if (instance.mobileCountryCode.length) {
        [dictionary setValue:instance.mobileCountryCode forKey:@"mcc"];
    }
    if (instance.mobileNetworkCode.length) {
        [dictionary setValue:instance.mobileNetworkCode forKey:@"mnc"];
    }
    if (instance.mobileCountryCode.length) {
        [dictionary setValue:instance.isoCountryCode forKey:@"country"];
    }
    
    return dictionary;
}

@end
