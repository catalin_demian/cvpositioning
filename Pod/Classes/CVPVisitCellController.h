//
//  CVPVisitCellController.h
//  Pods
//
//  Created by Catalin Demian on 2/19/16.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Tapptitude/TTBlockCollectionCellController.h>

@interface CVPVisitCellController : TTBlockCollectionCellController

@end
