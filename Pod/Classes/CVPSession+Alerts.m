//
//  Session+Alerts.m
//  Convoyz
//
//  Created by Andrei Dumitru on 7/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPSession+Alerts.h"
#import "CVPSession+CoreData.h"
#import "CVPLogEntry.h"
#import "SystemManager.h"

@implementation CVPSession (Alerts)

+ (void)showAlertForLocation:(CLLocation *)location trigger:(CVPLocationTrigger)trigger {
    if (!location) {
        return;
    }
    static NSDateFormatter *dateFormatter = nil;
    if (! dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    
    NSString *source = @"";
    switch (trigger) {
        case CVPLocationTriggerContinousMonitoring:
            break;
        case CVPLocationTriggerGeofence:
            source = @"geofence ";
            break;
        case CVPLocationTriggerSignificantChanges:
            source = @"significant changes ";
            break;
        case CVPLocationTriggerContinousMonitoring_LowAccuracy:
            source = @"LowAcc ";
            break;
        case CVPLocationTriggerContinousMonitoring_WiFiChanged:
            source = @"WiFi Changed ";
        case CVPLocationTriggerContinousMonitoring_MotionChanged:
            source = @"motion ";
        default:
            break;
    }
    
    static CLLocation *lastLocation = nil;
    NSString *locDistance = @"";
    if (lastLocation) {
        locDistance = [NSString stringWithFormat:@"%.0fm ", [location distanceFromLocation:lastLocation]];
    }
    lastLocation = location;
    
    NSString *locationInfo = NSStringFormat(@"<%.6f, %.6f>", location.coordinate.latitude, location.coordinate.longitude);
    NSString *title = NSStringFormat(@"%@%@%@, %.0f mps, %@", source, locDistance, locationInfo, location.speed, [dateFormatter stringFromDate:location.timestamp]);
    title = [NSString stringWithFormat:@"%@ 🔋%.0f%%", title, [[SystemManager instance] batteryLevel] * 100.0];
    [self showAlertWithTitle:title];
}

+ (void)showAlertForVisit:(CLVisit *)visit {
    if (!visit) {
        return;
    }
    
    static NSDateFormatter *dateFormatter = nil;
    if (! dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    NSString *arrival = [visit.arrivalDate isEqualToDate:[NSDate distantPast]] ? @"Past" : [dateFormatter stringFromDate:visit.arrivalDate];
    NSString *departure = [visit.departureDate isEqualToDate:[NSDate distantFuture]] ? @"Future" : [dateFormatter stringFromDate:visit.departureDate];
    
    NSString *source = @"Visit ";
    NSString *locationInfo = NSStringFormat(@"<%.6f, %.6f>", visit.coordinate.latitude, visit.coordinate.longitude);
    NSString *title = NSStringFormat(@"%@%@, [%@ - %@]", source, locationInfo, arrival, departure);
    title = [NSString stringWithFormat:@"%@ 🔋%.0f%%", title, [[SystemManager instance] batteryLevel] * 100.0];
    [self showAlertWithTitle:title source:CVPAlertSource.visit];
}

+ (void)showAlertForRegion:(CLRegion *)region {
    if (!region) {
        return;
    }
    NSString *title = NSStringFormat(@"Exit geofence with ID:%@", region.identifier);
    [self showAlertWithTitle:title];
}

+ (void)showAlertWithTitle:(NSString *)title source:(NSString *)source {
    TTLogExpr(title);
    
    NSDate *date = [NSDate date];
    
    NSString *appState = @"active";
    switch ([UIApplication sharedApplication].applicationState) {
        case UIApplicationStateInactive:
            appState = @"inactive";
            break;
        case UIApplicationStateBackground:
            appState = @"background";
            break;
        case UIApplicationStateActive:
        default:
            break;
    }
    
    [self saveDataInBackground:^id(NSManagedObjectContext *localContext) {
        CVPLogEntry *log = [CVPLogEntry insertInManagedObjectContext:localContext];
        log.date = date;
        log.message = title;
        log.appState = appState;
        if (source.length) {
            log.source = source;
        }
        return log;
    } completionBlock:^(id result, NSError *error) {
        
    }];
    
    if (![CVPSession showAlerts]) {
        return;
    }
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        //        [[UIApplication sharedApplication].delegate.window.rootViewController showSuccessMessage:title];
    } else if (self.isRegisteredForLocalNotifications){
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.alertBody = title;
        notification.soundName = @"Default";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }

}

+ (void)showAlertWithTitle:(NSString *)title {
    [self showAlertWithTitle:title source:nil];
}

+ (void)showAlertWithTitle:(NSString *)title date:(NSDate *)date {
    NSString *appState = @"active";
    switch ([UIApplication sharedApplication].applicationState) {
        case UIApplicationStateInactive:
            appState = @"inactive";
            break;
        case UIApplicationStateBackground:
            appState = @"background";
            break;
        case UIApplicationStateActive:
        default:
            break;
    }
    
    [self saveDataInBackground:^id(NSManagedObjectContext *localContext) {
        CVPLogEntry *log = [CVPLogEntry insertInManagedObjectContext:localContext];
        log.date = date;
        log.message = title;
        log.appState = appState;
        return log;
    } completionBlock:^(id result, NSError *error) {
        
    }];
}

#pragma mark - Helpers

+ (BOOL)isRegisteredForLocalNotifications {
    return UIApplication.sharedApplication.currentUserNotificationSettings.types != UIUserNotificationTypeNone;
}

@end
