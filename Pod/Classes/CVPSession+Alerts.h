//
//  Session+Alerts.h
//  Convoyz
//
//  Created by Andrei Dumitru on 7/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPSession.h"
#import <CoreLocation/CoreLocation.h>
#import "CVPLocationManager.h"

@interface CVPSession (Alerts)

+ (void)showAlertForLocation:(CLLocation *)location trigger:(CVPLocationTrigger)trigger;
+ (void)showAlertForVisit:(CLVisit *)visit;
+ (void)showAlertForRegion:(CLRegion *)region;
+ (void)showAlertWithTitle:(NSString *)title;
+ (void)showAlertWithTitle:(NSString *)title date:(NSDate *)date;

@end
