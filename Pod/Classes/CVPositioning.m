//
//  CVPPositioningManager.m
//  CVPositioning
//
//  Created by Catalin Demian on 2/3/16.
//  Copyright © 2016 Tapptitude. All rights reserved.
//

#import "CVPositioning.h"
#import "CVPLocationManager.h"
#import "CVPConstants.h"
#import "CVPMotionDetector.h"
#import "CVPSession+Alerts.h"
#import "CVPLocationSerializer.h"
#import "SystemInfoSerializer.h"
#import "CVPMotionSerializer.h"
#import "CVPSignificantLocationManager.h"
#import "CVPGeofencingLocationManager.h"
#import <CoreData/NSManagedObjectContext.h>
#import "MagicalRecord+Actions.h"
#import "CVPositioning+LocationManagers.h"
#import "CVPVisitExtensionsLocationManager.h"

@interface CVPositioning ()

@property (nonatomic, strong) NSMutableArray *observers;
@property (nonatomic, strong) NSMutableArray *locationCallbacks;
@property (assign, nonatomic) UIBackgroundTaskIdentifier backgroundTask;

@end

@implementation CVPositioning

+ (instancetype)globalInstance {
    static CVPositioning *globalInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        globalInstance = [[CVPositioning alloc] init];
    });
    
    return globalInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self registerForApplicationNotifications];
        self.locationCallbacks = [[NSMutableArray alloc] init];
        self.observers = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [self unregisterFromApplicationNotifications];
}

- (void)startUpdatingLocation {
    [[CVPMotionDetector instance] triggerAuthorizationCheck];
    
    if ([CVPSession continousMonitoringEnabled]) {
        [self locationManagerForTrigger:CVPLocationTriggerContinousMonitoring];
        [[CVPLocationManager continousInstance] startUpdatingLocation:@"autostart"];
    }
    
    if ([CVPSession significantChanges]) {
        [self locationManagerForTrigger:CVPLocationTriggerSignificantChanges];
        [[CVPSignificantLocationManager instance] startMonitoringSignificantLocationChanges];
    }
    
    if ([CVPSession geofenceEnabled]) {
        [self locationManagerForTrigger:CVPLocationTriggerGeofence];
        [[CVPGeofencingLocationManager instance] startRegionMonitoring];
    }
    
    if ([CVPSession visitsEnabled]) {
        [self locationManagerForTrigger:CVPLocationTriggerVisitExtension];
        [[CVPVisitExtensionsLocationManager instance] startMonitoringVisits];
    }

}

- (void)stopUpdatingLocation {
    if ([CVPSession continousMonitoringEnabled]) {
        [[CVPLocationManager continousInstance] stopUpdatingLocation];
        [[CVPLocationManager continousInstance] setLocationUpdateCallback:nil];
        continousInstance = nil;
    }
    
    if ([CVPSession geofenceEnabled]) {
        [[CVPGeofencingLocationManager instance] stopRegionMonitoring];
        [[CVPGeofencingLocationManager instance] setLocationUpdateCallback:nil];
        geofenceInstance = nil;
    }
    
    if ([CVPSession significantChanges]) {
        [[CVPSignificantLocationManager instance] stopMonitoringSignificantLocationChanges];
        [[CVPSignificantLocationManager instance] setLocationUpdateCallback:nil];
        significantInstance = nil;
    }
    
    if ([CVPSession visitsEnabled]) {
        [[CVPVisitExtensionsLocationManager instance] stopMonitoringVisits];
        [[CVPVisitExtensionsLocationManager instance] setVisitUpdateCallback:nil];
        visitInstance = nil;
    }

}

- (void)getCurrentLocationWithCallback:(void (^)(CLLocation *location, NSError *error))callback {
    CLLocation *location = [self lastKnownLocation];
    NSTimeInterval secondsFromLastLocation = [[NSDate date] timeIntervalSinceDate:[self dateForLastKnownLocation]];
    if (location && secondsFromLastLocation < CVPMinuteTimeInterval) {
        callback(location, nil);
        return;
    }
    
    [self.locationCallbacks addObject:callback];
}

#pragma mark - Observation

- (void)registerObserverForPositionUpdates:(id)observer {
    if (![self.observers containsObject:observer]) {
        [self.observers addObject:observer];
    }
}


- (void)unregisterObserver:(id)observer {
    if ([self.observers containsObject:observer]) {
        [self.observers removeObject:observer];
    }
}

- (void)notifyObserversOfLocationUpdates:(NSArray *)locations withTrigger:(CVPLocationTrigger)trigger {
    CLLocation *newLocation;
    if (locations.count) {
        newLocation = [locations lastObject];
        [self saveLocation:newLocation withTrigger:trigger];
    }
    
    [self.locationCallbacks enumerateObjectsUsingBlock:^(void(^callback)(CLLocation *location, NSError *error), NSUInteger idx, BOOL * _Nonnull stop) {
        callback(newLocation, nil);
    }];
    [self.locationCallbacks removeAllObjects];
    
    for (id<CVPositioningObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(didReceiveLocationUpdates:withTrigger:)]) {
            [observer didReceiveLocationUpdates:locations withTrigger:trigger];
        }
    }
}

- (void)notifyObserversOfError:(NSError *)error {
    [self.locationCallbacks enumerateObjectsUsingBlock:^(void(^callback)(CLLocation *location, NSError *error), NSUInteger idx, BOOL * _Nonnull stop) {
        callback(nil, error);
    }];
    [self.locationCallbacks removeAllObjects];
    
    for (id<CVPositioningObserver> observer in self.observers) {
        if ([observer respondsToSelector:@selector(didReceiveError:)]) {
            [observer didReceiveError:error];
        }
    }
}

#pragma mark - Store last location

- (void)saveLocation:(CLLocation *)location withTrigger:(CVPLocationTrigger)trigger {
    NSDictionary *locationDictionary = @{@"location":location,
                                         @"trigger":@(trigger),
                                         @"date":[NSDate date]};
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:locationDictionary];
    [[NSUserDefaults standardUserDefaults] setValue:data forKey:@"lastKnownLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary *)savedLocationDictionary {
    NSData *data = [[NSUserDefaults standardUserDefaults] dataForKey:@"lastKnownLocation"];
    NSDictionary *locationDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    return locationDictionary;
}

- (CVPLocationTrigger)triggerForLastKnownLocation {
    return [[[self savedLocationDictionary] objectForKey:@"trigger"] intValue];
}

- (CLLocation *)lastKnownLocation {
    return [[self savedLocationDictionary] objectForKey:@"location"];
}

- (NSDate *)dateForLastKnownLocation {
    return [[self savedLocationDictionary] objectForKey:@"date"];
}

#pragma mark - App Notifications

- (void)registerForApplicationNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidFinishLaunchingNotification:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminateNotification:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)unregisterFromApplicationNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)appDidFinishLaunchingNotification:(NSNotification *)notification {
    NSDate *killedDate = [CVPSession applicationKillDate];
    if (killedDate) {
        TTLogExpr(killedDate);
        [CVPSession showAlertWithTitle:@"==== Application Killed ===" date:killedDate];
    }
    [CVPSession setApplicationKillDate:nil];
    
    [CVPSession showAlertWithTitle:[NSString stringWithFormat:@""]];
    [CVPSession showAlertWithTitle:[NSString stringWithFormat:@"------ Application started------"]];
    
    if (notification.userInfo[UIApplicationLaunchOptionsLocationKey]) {
        NSString *message = [NSString stringWithFormat:@"Launched from location update: %@", notification.userInfo[UIApplicationLaunchOptionsLocationKey]];
        [CVPSession showAlertWithTitle:NSLocalizedString(message, nil)];
        [self applicationBeginBackgroundTaskExecution:[UIApplication sharedApplication]];
    }
}

- (void)appWillTerminateNotification:(NSNotification *)notification {
    [CVPSession setApplicationKillDate:[NSDate date]];
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    if (self.backgroundTask == UIBackgroundTaskInvalid) {
        return;
    }
    [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
    self.backgroundTask = UIBackgroundTaskInvalid;
}

- (void)appDidEnterBackground:(NSNotification *)notification {
    [self applicationBeginBackgroundTaskExecution:[UIApplication sharedApplication]];
}

- (void)applicationBeginBackgroundTaskExecution:(UIApplication *)application {
    
    self.backgroundTask = [application beginBackgroundTaskWithExpirationHandler:^{
        [application endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }];
}

@end
