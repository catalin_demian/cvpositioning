// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CVPHarvest.h instead.

#import <CoreData/CoreData.h>

extern const struct CVPHarvestAttributes {
	__unsafe_unretained NSString *content;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *harvestID;
	__unsafe_unretained NSString *priority;
	__unsafe_unretained NSString *source;
	__unsafe_unretained NSString *status;
	__unsafe_unretained NSString *timezone;
	__unsafe_unretained NSString *type;
} CVPHarvestAttributes;

@class NSObject;

@interface CVPHarvestID : NSManagedObjectID {}
@end

@interface _CVPHarvest : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CVPHarvestID* objectID;

@property (nonatomic, strong) id content;

//- (BOOL)validateContent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* harvestID;

//- (BOOL)validateHarvestID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* priority;

@property (atomic) int32_t priorityValue;
- (int32_t)priorityValue;
- (void)setPriorityValue:(int32_t)value_;

//- (BOOL)validatePriority:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* source;

@property (atomic) int32_t sourceValue;
- (int32_t)sourceValue;
- (void)setSourceValue:(int32_t)value_;

//- (BOOL)validateSource:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* status;

@property (atomic) int32_t statusValue;
- (int32_t)statusValue;
- (void)setStatusValue:(int32_t)value_;

//- (BOOL)validateStatus:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* timezone;

//- (BOOL)validateTimezone:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* type;

@property (atomic) int32_t typeValue;
- (int32_t)typeValue;
- (void)setTypeValue:(int32_t)value_;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

@end

@interface _CVPHarvest (CoreDataGeneratedPrimitiveAccessors)

- (id)primitiveContent;
- (void)setPrimitiveContent:(id)value;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSString*)primitiveHarvestID;
- (void)setPrimitiveHarvestID:(NSString*)value;

- (NSNumber*)primitivePriority;
- (void)setPrimitivePriority:(NSNumber*)value;

- (int32_t)primitivePriorityValue;
- (void)setPrimitivePriorityValue:(int32_t)value_;

- (NSNumber*)primitiveSource;
- (void)setPrimitiveSource:(NSNumber*)value;

- (int32_t)primitiveSourceValue;
- (void)setPrimitiveSourceValue:(int32_t)value_;

- (NSNumber*)primitiveStatus;
- (void)setPrimitiveStatus:(NSNumber*)value;

- (int32_t)primitiveStatusValue;
- (void)setPrimitiveStatusValue:(int32_t)value_;

- (NSString*)primitiveTimezone;
- (void)setPrimitiveTimezone:(NSString*)value;

@end
