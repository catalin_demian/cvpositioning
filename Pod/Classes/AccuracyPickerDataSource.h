//
//  AccuracyPickerDataSource.h
//  Positioning
//
//  Created by Alexandru Tudose on 02/11/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AccuracyPickerDataSource : NSObject <UIPickerViewDataSource, UIPickerViewDelegate>

- (CLLocationAccuracy)accuracyForName:(NSString *)accuracyName;
- (NSString *)nameForAccuracy:(CLLocationAccuracy)accuracy;
- (NSString *)nameForIndex:(NSInteger)index;

@end
