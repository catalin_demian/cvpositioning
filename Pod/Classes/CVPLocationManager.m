//
//  LocationManager.m
//  Positioning
//
//  Created by Andrei Dumitru on 10/6/15.
//  Copyright © 2015 Tapptitude. All rights reserved.
//

#import "CVPLocationManager.h"
#import "SystemManager.h"
#import "SystemManager+Network.h"
#import "CVPMotionDetector.h"
//#import "Debug.h"
//#import "EXTScope.h"
#import "CVPSession+Alerts.h"
#import "NSError+CoreLocation.h"

@interface CVPLocationManager () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) BOOL isDefferedUpdatesOn;

@property (nonatomic, assign) BOOL motionDetected;
@property (nonatomic, assign) BOOL wifiChanged;

@property (strong, nonatomic) CLLocation *lastLocation;
@property (nonatomic, strong) NSString *wifiBSSID;

@property (nonatomic, strong) NSTimer *restartTimer;
@property (nonatomic, strong) NSTimer *stopTimer;

@property (nonatomic, strong) NSTimer *missingMotionTimer;

@property (nonatomic, strong) NSTimer *checkAccurracyTimer;

@property (nonatomic, strong) NSMutableArray *deferredLocations;
@end

@implementation CVPLocationManager

- (instancetype)initForContinousMonitoring {
    if (self = [super init]) {
        
        self.triggerMode = CVPLocationTriggerContinousMonitoring;
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = [CVPSession locationAccuracy].doubleValue;
        self.locationManager.activityType = [CVPSession activityType].integerValue;
        self.locationManager.distanceFilter = [CVPSession updateDistance].floatValue;
        self.locationManager.pausesLocationUpdatesAutomatically = [CVPSession autoPauseLocationByiOSEnabled];
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]) {
            self.locationManager.allowsBackgroundLocationUpdates = YES;
        }
        
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForegroundNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackgroundNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        self.wifiBSSID = [[SystemManager instance] BSSIDString];
    }
    return self;
}


CVPLocationManager *continousInstance = nil;

+ (instancetype)continousInstance {
    return continousInstance;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.stopTimer invalidate];
    self.stopTimer = nil;
    [self.restartTimer invalidate];
    self.restartTimer = nil;
}





#pragma mark - Getters/Setters

- (BOOL)isAuthorized {
    return !(CLLocationManager.authorizationStatus == kCLAuthorizationStatusRestricted || CLLocationManager.authorizationStatus == kCLAuthorizationStatusDenied);
}

- (BOOL)locationServicesEnabled {
    return [CLLocationManager locationServicesEnabled];
}

- (CLLocationDistance)distanceFilter {
    return self.locationManager.distanceFilter;
}

- (void)setDistanceFilter:(CLLocationDistance)distanceFilter {
    self.locationManager.distanceFilter = distanceFilter;
}

- (CLActivityType)activityType {
    return self.locationManager.activityType;
}

- (void)setActivityType:(CLActivityType)activityType {
    self.locationManager.activityType = activityType;
}

- (void)setLocationAccuracy:(CLLocationAccuracy)accuracy {
    self.locationManager.desiredAccuracy = accuracy;
}

#pragma mark - Public methods

- (void)startUpdatingLocation:(NSString *)reason {
    [self.missingMotionTimer invalidate];
    self.missingMotionTimer = nil;
    
    self.isDecreasedMonitoringQualityOn = NO;
    self.locationManager.delegate = self;
    if (self.locationManager.desiredAccuracy != [CVPSession locationAccuracy].doubleValue) {
        NSString *message = [NSString stringWithFormat:@"Increased monitoring quality - %@", reason];
        [CVPSession showAlertWithTitle:message];
    } else {
        NSString *message = [NSString stringWithFormat:@"Start updating - %@", reason];
        [CVPSession showAlertWithTitle:message];
    }
    self.locationManager.desiredAccuracy = [CVPSession locationAccuracy].doubleValue;
    
    self.locationManager.activityType = [CVPSession activityType].integerValue;
    self.locationManager.distanceFilter = [CVPSession updateDistance].floatValue;
    [self.locationManager requestAlwaysAuthorization];
    
    if ([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]) {
        self.locationManager.allowsBackgroundLocationUpdates = YES;
    }
    
    [self.locationManager startUpdatingLocation];
    self.updatingLocation = YES;
    
    [[CVPMotionDetector instance] setMotionDetectedCallback:nil];
}

- (void)stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
    self.updatingLocation = NO;
}




- (void)activateDeferredUpdates {
//    http://stackoverflow.com/questions/23351108/ios-deferred-location-updates-fail-to-defer
    [self allowDeferredLocationUpdatesUntilTraveled:[CVPSession deferDistance].doubleValue timeout:[CVPSession deferTimeInterval].doubleValue];
//    [self allowDeferredLocationUpdatesUntilTraveled:CLLocationDistanceMax timeout:CLTimeIntervalMax];
}

- (void)allowDeferredLocationUpdatesUntilTraveled:(CLLocationDistance)distance timeout:(NSTimeInterval)timeout {
    if (![CLLocationManager deferredLocationUpdatesAvailable]) {
        return;
    }
    
    static NSDate *previousActivateDate = nil;
    if (previousActivateDate && fabs([previousActivateDate timeIntervalSinceNow]) < 1.0) {
        return;
    }
    
    self.deferredLocations = [NSMutableArray array];
    previousActivateDate = [NSDate date];
    self.isDefferedUpdatesOn = YES;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager allowDeferredLocationUpdatesUntilTraveled:distance timeout:timeout];
}

- (void)disallowDeferredLocationUpdates {
    if (![CLLocationManager deferredLocationUpdatesAvailable]) {
        return;
    }
    
    [self.locationManager disallowDeferredLocationUpdates];
    self.locationManager.distanceFilter = [CVPSession updateDistance].doubleValue;
    self.isDefferedUpdatesOn = NO;
}

- (void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(nullable NSError *)error {
    if (error) {
        [CVPSession showAlertWithTitle:[NSString stringWithFormat:@"%@, locationsCount %ld", [error failedReason], self.deferredLocations.count]];
    } else {
        NSString *message = NSStringFormat(@"Did finish deferred updates locationsCount %ld", self.deferredLocations.count);
        [CVPSession showAlertWithTitle:message];
    }
    [self.deferredLocations removeAllObjects];
    
    [self disallowDeferredLocationUpdates];
}










#pragma mark - Notifications

- (void)applicationWillEnterForegroundNotification:(NSNotification *)notification {
    [self.restartTimer invalidate];
    self.restartTimer = nil;
    [self.stopTimer invalidate];
    self.stopTimer = nil;
    
    [self.checkAccurracyTimer invalidate];
    self.checkAccurracyTimer = nil;
    
    if (self.isDefferedUpdatesOn) {
        return;
    }
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    if (!self.isUpdatingLocation) {
        [self startUpdatingLocation:@"app did become active"];
//    }
}

- (void)applicationDidEnterBackgroundNotification:(NSNotification *)notification {
    [self.checkAccurracyTimer invalidate];
    self.checkAccurracyTimer = nil;
    if ([CVPSession decreaseAccuracyEnabled]) {
        NSTimeInterval interval = [CVPSession checkAccuracyTimeInterval].doubleValue;
        self.checkAccurracyTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(checkForChangesInWifi) userInfo:nil repeats:YES];
    }
}







#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSMutableArray *filterdLocations = [NSMutableArray array];
    for (CLLocation *location in locations) {
        if (location.horizontalAccuracy <= 70 || location.horizontalAccuracy < 0.0) {
            [filterdLocations addObject:location];
        } else {
            TTLog(@"Ignored location because horizontal accuracy is low : %.1f", location.horizontalAccuracy);
        }
    }
    if (! filterdLocations) {
        return;
    }
    locations = filterdLocations;
    
    CLLocation *location = [locations lastObject];
    if ([CVPSession deferEnabled]) {
        if (! self.isDefferedUpdatesOn) {
            if (! self.isDecreasedMonitoringQualityOn) {
                [self activateDeferredUpdates];
            }
        } else {
            BOOL fromDeffered = fabs([self.lastLocation.timestamp timeIntervalSinceNow]) > 3.0;
            if (fromDeffered) {
                [self.deferredLocations addObjectsFromArray:locations];
            }
        }
        
        TTLogExpr([self.lastLocation distanceFromLocation:location]);
        if (self.lastLocation && [self.lastLocation distanceFromLocation:location] < [CVPSession updateDistance].doubleValue) {
            return;
        }
    }
    
    self.lastLocation = location;
    
    CVPLocationTrigger trigger = self.triggerMode;
    if (self.wifiChanged) {
        self.wifiChanged = NO;
        trigger = CVPLocationTriggerContinousMonitoring_WiFiChanged;
    } else if (self.motionDetected) {
        self.motionDetected = NO;
        trigger = CVPLocationTriggerContinousMonitoring_MotionChanged;
    } else if (kCLLocationAccuracyThreeKilometers == manager.desiredAccuracy) {
        trigger = CVPLocationTriggerContinousMonitoring_LowAccuracy;
    }
    
    if (self.locationUpdateCallback) {
        self.locationUpdateCallback(locations, trigger);
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (self.isUpdatingLocation) {
        self.updatingLocation = NO;
    }
    self.motionDetected = NO;
    self.wifiChanged = NO;
    
    self.errorCallback(error);
    [CVPSession showAlertWithTitle:NSStringFormat(@"Continuous monitoring did fail %@", [error localizedDescription])];
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager {
    [CVPSession showAlertWithTitle:@"Location manager did pause"];
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager {
    [CVPSession showAlertWithTitle:@"Location manager did resume"];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
}










#pragma mark - Helpers

- (BOOL)isConnectedToSameWifi {
    NSString *wifiMac = [[SystemManager instance] BSSIDString];
    if (self.wifiBSSID && [self.wifiBSSID isEqualToString:wifiMac]) {
        return YES;
    }
    
    return NO;
}

- (void)automaticStopLocationUpdates {
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
        return;
    }
    
    if ([CVPSession significantChanges] || [CVPSession geofenceEnabled]) {
    } else {
        [self.restartTimer invalidate];
        [self.stopTimer invalidate];
        self.restartTimer = nil;
        self.stopTimer = nil;
        
//        if ([Session decreaseAccuracyEnabled]) {
//            CLLocation *location = [self.locationManager location];
//            BOOL shouldStop = [self.lastLocation distanceFromLocation:location] < [Session updateDistance].doubleValue;
//            
//            if (shouldStop && [self isConnectedToSameWifi] && ! self.isDecreasedMonitoringQualityOn) {
//                [self decreaseMonitoringQuality];
//                return;
//            }
//        }
        
//        double restartAfter = [Session restartMonitoringTimeInterval].doubleValue;
//        if (restartAfter > 0.0) {
//            self.restartTimer = [NSTimer scheduledTimerWithTimeInterval:restartAfter target:self selector:@selector(startUpdatingLocation) userInfo:nil repeats:NO];
//            self.stopTimer = [NSTimer scheduledTimerWithTimeInterval:BackgroundMonitoringTimeInterval target:self selector:@selector(stopUpdatingLocationWhenThereAreNoChanges) userInfo:nil repeats:NO];
//        }
    }
}

//- (void)stopUpdatingLocationWhenThereAreNoChanges {
//    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
//        return;
//    }
//    
//    CLLocation *location = [self.locationManager location];
//    BOOL shouldStop = [self.lastLocation distanceFromLocation:location] < [Session updateDistance].doubleValue;
//    TTLogExpr([self.lastLocation distanceFromLocation:location]);
//    if (shouldStop) {
//        if (self.locationManager.desiredAccuracy == kCLLocationAccuracyThreeKilometers) {
//            return;
//        }
//        
//        [Session showAlertWithTitle:@"Decreased monitoring quality"];
//        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
//        [self.locationManager setDistanceFilter:99999];
//    }
//}


- (void)checkForChangesInWifi {
    if (! [CVPSession decreaseAccuracyEnabled]) {
        return;
    }
    
    static CLLocation *lastLocation = nil;
    if (! lastLocation) {
        lastLocation = self.lastLocation;
        return;
    }
    
    CLLocation *location = self.lastLocation;
    BOOL shouldStop = [lastLocation distanceFromLocation:location] < [CVPSession updateDistance].doubleValue;
    
    if (shouldStop) {
        NSTimeInterval minutes = [CVPSession monitoringBufferInterval].doubleValue;
        BOOL didPass15Min = fabs([lastLocation.timestamp timeIntervalSinceNow]) > minutes;
        shouldStop = didPass15Min;
    } else {
        lastLocation = self.lastLocation;
    }
    
    
    NSString *wifiBSSID = [[SystemManager instance] BSSIDString];
    
    BOOL noWifi = wifiBSSID.length < 1;
    BOOL wasConnectedToWifi = self.wifiBSSID.length > 0;
    BOOL sameWifi = [self isConnectedToSameWifi];
    self.wifiBSSID = wifiBSSID;
    
    if (noWifi) {
        if (wasConnectedToWifi) {
            self.wifiChanged = YES;
            [self startUpdatingLocation:@"Disconnected from wifi"];
        } else if (self.isDecreasedMonitoringQualityOn) {
            // we are getting notification instead
//            [[MotionDetector instance] didMovedInLastHour:^(BOOL didMove) {
//                if (didMove && self.isDecreasedMonitoringQualityOn) {
//                    [self startUpdatingLocation];
//                }
//            }];
            
            
        } else if (shouldStop && ! self.isDecreasedMonitoringQualityOn) {
//            [Session showAlertWithTitle:@"No movement from gps, checking for motion"];
//            
//            [[MotionDetector instance] didMovedInLastHour:^(BOOL didMove) {
//                if (! didMove && ! self.isDecreasedMonitoringQualityOn) {
//                    float minutes = fabs([lastLocation.timestamp timeIntervalSinceNow]) / MinuteTimeInterval;
//                    NSString *message = [NSString stringWithFormat:@"no movement in %.fmin", minutes];
//                    [self decreaseMonitoringQualityWithTrigger:message];
//                }
//            }];
            
            [CVPSession showAlertWithTitle:@"No movement from gps"];
            float minutes = fabs([lastLocation.timestamp timeIntervalSinceNow]) / CVPMinuteTimeInterval;
            NSString *message = [NSString stringWithFormat:@"no movement in %.fmin", minutes];
            [self decreaseMonitoringQualityWithTrigger:message];
            
        }
    } else if (sameWifi) {
        if (shouldStop && ! self.isDecreasedMonitoringQualityOn) {
            [self decreaseMonitoringQualityWithTrigger:@"same wifi"];
        }
    } else {
        if (self.isDecreasedMonitoringQualityOn) {
            self.wifiChanged = YES;
            [self startUpdatingLocation:@"connected to a different wifi"];
        }
    }
}

- (void)decreaseMonitoringQualityWithTrigger:(NSString *)trigger {
    self.isDecreasedMonitoringQualityOn = YES;
    if (self.locationManager.desiredAccuracy == kCLLocationAccuracyThreeKilometers) {
        return;
    }
    
    if (self.lastLocation && self.locationUpdateCallback) {
        CLLocation *location = self.lastLocation;
        CVPLocationTrigger trigger = self.triggerMode;
        self.locationUpdateCallback(@[location], trigger);
    }
    
    NSString *message = [NSString stringWithFormat:@"Decreased monitoring quality - %@", trigger];
    [CVPSession showAlertWithTitle:message];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
    [self.locationManager setDistanceFilter:99999];
    [self.locationManager startUpdatingLocation];
    
    if ([CVPSession motionProcessorEnabled]) {
        @weakify(self);
        [[CVPMotionDetector instance] setMotionDetectedCallback:^(NSString *reason) {
            @strongify(self)
            self.motionDetected = YES;
            [self startUpdatingLocation:@"motion changed"];
        }];
    } else {
        [CVPSession showAlertWithTitle:@"Motion activity detection - is disabled"];
    }

    
    if (! [CMMotionActivityManager isActivityAvailable]) {
        NSTimeInterval minutes = [CVPSession monitoringBufferInterval].doubleValue;
        [self.missingMotionTimer invalidate];
        self.missingMotionTimer = [NSTimer scheduledTimerWithTimeInterval:minutes target:self selector:@selector(missingMotionTimerTrigger:) userInfo:nil repeats:NO];
    }
}

- (void)missingMotionTimerTrigger:(id)sender {
    [self.missingMotionTimer invalidate];
    self.missingMotionTimer = nil;
    
    if (! self.isUpdatingLocation) {
        [self startUpdatingLocation:@"Motion Hardware N/A"];
    }
}

- (void)setAutoPauseByiOS:(BOOL)autoPause {
    self.locationManager.pausesLocationUpdatesAutomatically = autoPause;
    if (autoPause != self.locationManager.pausesLocationUpdatesAutomatically) {
        NSString *message = [NSString stringWithFormat:@"AutoPause value did not changed to %d, and current value is %d", autoPause, self.locationManager.pausesLocationUpdatesAutomatically];
        [[[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show
         ];
    }
}

@end
