//
//  MotionSerializer.m
//  Convoyz
//
//  Created by Andrei Dumitru on 6/22/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "CVPMotionSerializer.h"
#import "NSDate+Timestamp.h"
#import "CMMotionActivity+Custom.h"

@implementation CVPMotionSerializer

+ (NSDictionary *)serializePedometerData:(CMPedometerData *)pedometerData {
    if (!pedometerData) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(pedometerData.startDate.timestamp) forKey:@"startDate"];
    [dictionary setValue:@(pedometerData.endDate.timestamp) forKey:@"endDate"];
    [dictionary setValue:pedometerData.numberOfSteps forKey:@"numberOfSteps"];
    [dictionary setValue:pedometerData.distance forKey:@"distance"];
    [dictionary setValue:pedometerData.floorsAscended forKey:@"floorsAscended"];
    [dictionary setValue:pedometerData.floorsDescended forKey:@"floorsDescended"];
    return dictionary;
}

+ (NSDictionary *)serializeAltitudeData:(CMAltitudeData *)altitudeData {
    if (!altitudeData) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:altitudeData.relativeAltitude forKey:@"relativeAltitude"];
    [dictionary setValue:altitudeData.pressure forKey:@"pressure"];
    return dictionary;
}

@end

@implementation CVPMotionSerializer (Motion)

+ (NSDictionary *)serializeAccelerometerData:(CMAccelerometerData *)accelerometerData {
    if (!accelerometerData) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(accelerometerData.timestamp) forKey:@"timestamp"];
    [dictionary setValue:@(accelerometerData.acceleration.x) forKey:@"accelerationX"];
    [dictionary setValue:@(accelerometerData.acceleration.y) forKey:@"accelerationY"];
    [dictionary setValue:@(accelerometerData.acceleration.z) forKey:@"accelerationZ"];

    return dictionary;
}

+ (NSDictionary *)serializeGyroData:(CMGyroData *)gyroData {
    if (!gyroData) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(gyroData.timestamp) forKey:@"timestamp"];
    [dictionary setValue:@(gyroData.rotationRate.x) forKey:@"rotationRateX"];
    [dictionary setValue:@(gyroData.rotationRate.y) forKey:@"rotationRateY"];
    [dictionary setValue:@(gyroData.rotationRate.z) forKey:@"rotationRateZ"];
    
    return dictionary;
}

+ (NSDictionary *)serializeMagnetometerData:(CMMagnetometerData *)magnetometerData {
    if (!magnetometerData) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(magnetometerData.timestamp) forKey:@"timestamp"];
    [dictionary setValue:@(magnetometerData.magneticField.x) forKey:@"magneticFieldX"];
    [dictionary setValue:@(magnetometerData.magneticField.y) forKey:@"magneticFieldY"];
    [dictionary setValue:@(magnetometerData.magneticField.z) forKey:@"magneticFieldZ"];
    
    return dictionary;
}

+ (NSDictionary *)serializeDeviceMotion:(CMDeviceMotion *)deviceMotion {
    if (!deviceMotion) {
        return nil;
    }
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:@(deviceMotion.timestamp) forKey:@"timestamp"];
    [dictionary setValue:@(deviceMotion.gravity.x) forKey:@"gravityX"];
    [dictionary setValue:@(deviceMotion.gravity.y) forKey:@"gravityY"];
    [dictionary setValue:@(deviceMotion.gravity.z) forKey:@"gravityZ"];
    [dictionary setValue:@(deviceMotion.userAcceleration.x) forKey:@"userAccelerationX"];
    [dictionary setValue:@(deviceMotion.userAcceleration.y) forKey:@"userAccelerationY"];
    [dictionary setValue:@(deviceMotion.userAcceleration.z) forKey:@"userAccelerationZ"];
    [dictionary setValue:@(deviceMotion.rotationRate.x) forKey:@"rotationRateX"];
    [dictionary setValue:@(deviceMotion.rotationRate.y) forKey:@"rotationRateY"];
    [dictionary setValue:@(deviceMotion.rotationRate.z) forKey:@"rotationRateZ"];
    return dictionary;
}

@end


@implementation CVPMotionSerializer (Activity)

+ (NSArray *)serializeActivities:(NSArray *)content {
    NSMutableArray *dictionaries = [[NSMutableArray alloc] init];
    
    for (CMMotionActivity *activity in content) {
        NSDictionary *dictionary = [self serializeActivity:activity];
        if (dictionary) {
            [dictionaries addObject:dictionary];
        }
    }
    return dictionaries;
}

+ (NSDictionary *)serializeActivity:(CMMotionActivity *)activity {
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    [dictionary setValue:activity.confidenceString forKey:@"confidence"];
    [dictionary setValue:@(activity.startDate.timestamp) forKey:@"startDate"];
    [dictionary setValue:@(activity.unknown) forKey:@"unknown"];
    [dictionary setValue:@(activity.stationary) forKey:@"stationary"];
    [dictionary setValue:@(activity.walking) forKey:@"walking"];
    [dictionary setValue:@(activity.running) forKey:@"running"];
    [dictionary setValue:@(activity.automotive) forKey:@"automotive"];
    [dictionary setValue:@(activity.cycling) forKey:@"cycling"];
    
    return dictionary;
}

@end
