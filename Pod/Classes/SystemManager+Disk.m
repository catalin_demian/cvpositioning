//
//  System+Disk.m
//  Convoyz
//
//  Created by Andrei Dumitru on 5/20/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager+Disk.h"

#define MB (1024*1024)
#define GB (MB*1024)

@implementation SystemManager (Disk)

- (NSString *)diskSpaceString {
    double diskSpace = self.diskSpace.doubleValue;
    if (diskSpace <= 0.0) {
        return nil;
    }
    return [self formatMemorySpace:@(diskSpace)];
}

- (NSString *)freeDiskSpaceString:(BOOL)inPercent {
    double freeDiskSpace = self.freeDiskSpace.doubleValue;
    if (freeDiskSpace <= 0.0) {
        return nil;
    }
    NSString *string = nil;
    if (inPercent) {
        double percentDiskSpace = (freeDiskSpace * 100) / self.diskSpace.doubleValue;
        string = percentDiskSpace <= 0 ? nil : [NSString stringWithFormat:@"%.2f%%", percentDiskSpace];
    } else {
        string = [self formatMemorySpace:@(freeDiskSpace)];
    }
    return string;
}

- (NSString *)usedDiskSpaceString:(BOOL)inPercent {
    double usedDiskSpace = self.usedDiskSpace.doubleValue;
    double totalDiskSpace = self.diskSpace.doubleValue;
    double freeDiskSpace = self.freeDiskSpace.doubleValue;
    
    if (totalDiskSpace <= 0.0 || freeDiskSpace <= 0.0 || usedDiskSpace <= 0.0) {
        return nil;
    }
    
    NSString *string = nil;
    if (inPercent) {
        float percentUsedDiskSpace = (usedDiskSpace * 100) / totalDiskSpace;
        string = percentUsedDiskSpace <= 0.0 ? nil : [NSString stringWithFormat:@"%.2f%%", percentUsedDiskSpace];
    } else {
        string = [self formatMemorySpace:@(usedDiskSpace)];
    }
    return string;
}

#pragma mark - Disk Information

- (NSNumber *)usedDiskSpace {
    long long usedSpace = self.diskSpace.longLongValue - self.freeDiskSpace.longLongValue;
    if (usedSpace <= 0) {
        return nil;
    }
    return @(usedSpace);
}

- (NSNumber *)diskSpace {
    NSError *error = nil;
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:&error];
    if (error) {
        return nil;
    }
    return [attributes objectForKey:NSFileSystemSize];
}

- (NSNumber *)freeDiskSpace {
    NSError *error = nil;
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:&error];
    if (error) {
        return nil;
    }
    return [attributes objectForKey:NSFileSystemFreeSize];
}

#pragma mark - Helpers

- (NSString *)formatMemorySpace:(NSNumber *)space {
    NSString *string = nil;
    
    double bytesCount = 1.0 * space.doubleValue;
    double gigaBytesCount = bytesCount / GB;
    double megaBytesCount = bytesCount / MB;
    
    if (gigaBytesCount >= 1.0) {
        string = [NSString stringWithFormat:@"%.2f GB", gigaBytesCount];
    } else if (megaBytesCount >= 1) {
        string = [NSString stringWithFormat:@"%.2f MB", megaBytesCount];
    } else {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setPositiveFormat:@"###,###,###,###"];
        
        string = [formatter stringFromNumber:space];
        string = [string stringByAppendingString:@" bytes"];
    }
    return string;
}

@end
