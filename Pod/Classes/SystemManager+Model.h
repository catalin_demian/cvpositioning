//
//  System+Model.h
//  Convoyz
//
//  Created by Andrei Dumitru on 5/21/15.
//  Copyright (c) 2015 Tapptitude. All rights reserved.
//

#import "SystemManager.h"

@interface SystemManager (Model)

@property (readonly, nonatomic) NSString *deviceModel;
@property (readonly, nonatomic) NSString *deviceName;
@property (readonly, nonatomic) NSString *deviceType;
@property (readonly, nonatomic) NSString *systemName;
@property (readonly, nonatomic) NSString *systemVersion;

@end
