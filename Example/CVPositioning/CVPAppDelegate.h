//
//  CVPAppDelegate.h
//  CVPositioning
//
//  Created by Demian Catalin on 02/04/2016.
//  Copyright (c) 2016 Demian Catalin. All rights reserved.
//

@import UIKit;

@interface CVPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
