//
//  main.m
//  CVPositioning
//
//  Created by Demian Catalin on 02/04/2016.
//  Copyright (c) 2016 Demian Catalin. All rights reserved.
//

@import UIKit;
#import "CVPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CVPAppDelegate class]));
    }
}
