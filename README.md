# CVPositioning

[![CI Status](http://img.shields.io/travis/Demian Catalin/CVPositioning.svg?style=flat)](https://travis-ci.org/Demian Catalin/CVPositioning)
[![Version](https://img.shields.io/cocoapods/v/CVPositioning.svg?style=flat)](http://cocoapods.org/pods/CVPositioning)
[![License](https://img.shields.io/cocoapods/l/CVPositioning.svg?style=flat)](http://cocoapods.org/pods/CVPositioning)
[![Platform](https://img.shields.io/cocoapods/p/CVPositioning.svg?style=flat)](http://cocoapods.org/pods/CVPositioning)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CVPositioning is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CVPositioning"
```

## Author

Demian Catalin, Catalin.Flaviu.Demian@gmail.com

## License

CVPositioning is available under the MIT license. See the LICENSE file for more info.
