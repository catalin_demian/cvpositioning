Pod::Spec.new do |s|
    s.name             = "CVPositioning"
    s.version          = "1.0.0"
    s.summary          = "A library used for optimized location tracking"
    s.description      = "The CVPositioning library will tune your app so that it tracks detailed motion information with minimum power consumption by alternating between different accuracy levels based on the current connectivity and the feedback from the motion sensor."
    s.author           = { "Demian Catalin" => "Catalin.Flaviu.Demian@gmail.com" }
    s.source           = { :git => "https://bitbucket.org/catalin_demian/cvpositioning.git" }
    s.platform     = :ios, '7.0'
    s.requires_arc = true
    s.source_files = 'Pod/Classes/*.{h,m}'
    s.resource_bundles = {
                        'CVPositioning' => ['Pod/Assets/*']
                        }
    s.resources = 'Pod/Classes/**/*.{xib}', 'Pod/Assets/*'
    s.frameworks = 'UIKit', 'CoreData', 'QuartzCore', 'SystemConfiguration', 'CoreGraphics', 'ImageIO', 'CoreLocation', 'CoreMotion', 'CoreTelephony'
    s.dependency 'Tapptitude'
    s.dependency 'MagicalRecord'
    s.prefix_header_contents = '#import <UIKit/UIKit.h>
                                #import <Tapptitude/Tapptitude.h>
                                #import <MagicalRecord/MagicalRecord.h>'
end
